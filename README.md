# resilient_teaming_example
Resilient Teaming MATLAB Example

## Initialize a workspace and build the code
Clone and build this repository and the [resilient_team_planner](https://gitlab.com/barton-research-group/open/resilient_team_planner).
```bash
cd ~
mkdir teaming_ws
cd teaming_ws
git clone git@gitlab.com:barton-research-group/open/resilient_teaming_example.git
git clone git@gitlab.com:barton-research-group/open/resilient_team_planner.git
# Build the binaries for resilient_teaming_example
cd resilient_teaming_example
mkdir temp # To store the temporarily generated figures
mkdir build
cd build
cmake ..
make
cd ../../
# Build the binaries for resilient_team_planner
cd resilient_team_planner
mkdir build
cd build
cmake ..
make
cd ../../resilient_teaming_example
```

## Run flow decomposition and plot
For the definition of a flow decomposition problem check Sec. IV in \[[2](https://arxiv.org/pdf/2106.12111.pdf)\].

In short, a flow decomposition problem rounds a real-valued network flow to integer valued network flow, and then splits the integer flow into paths of vehicles.

An example of a flow rounding is shown below (the flow constraints should be maintained during the rounding process).

<img src="readme/flow_round_result.png" alt="flow_round_result" width="40%"/>

Go to the main directory of `resilient_teaming_example` project.
```bash
ex6_flow # Run this in Matlab, it will generate the workspace/test/graph.yaml
./flow_main # Make and run this in workspace/resilient_team_planner/build/ and output cover.yaml and round.yaml in workspace/result/
ex6_plotter # Run this in Matlab
```

## Run the teaming case with energy uncertainty

An offroad multi-agent mission described in Section IV-B in \[[1](https://arxiv.org/pdf/2010.11376.pdf)\].

<img src="readme/energy_case.png" alt="energy_case" width="40%"/>

```matlab
map_gen
test_gpr
offroadcase_case_gen % Generate a test case consists of 14 tasks, 6 agent species, and 8 types of capabilities 
```
Output in the Matlab console will be `./main ../../test/energy/int_v18_m14_a1/planner_param.yaml ../../result/energy/int_v18_m14_a1/result.yaml`.

This command should be run in the directory `~/teaming_ws/resilient_team_planner/build/`.

The optimization problem will be written to the files in the folder `../test/energy/int_v18_m14_a1/`.

The next step is to use the executable the above command in the directory of `../resilient_team_planner/build/` to find a teaming plan for the optimization problem.

The planning results will be output to the Linux command window as well as saved in `../result/energy/int_v18_m14_a1/result.yaml`.

### Multiple planning modes

Three planning modes have been run on this test case example.

Change the `flagSolver: TEAMPLANNER_DET` in `../test/energy/int_v18_m14_a1/planner_param.yaml` to see other results.

```bash
cd ~/teaming_ws/resilient_team_planner/build/
./main ../../test/energy/int_v18_m14_a1/planner_param.yaml ../../result/energy/int_v18_m14_a1/result.yaml
# Before executing the next line, change the flagSolver: TEAMPLANNER_DET to TEAMPLANNER_CCP in ../test/energy/int_v18_m14_a1/planner_param.yaml
./main ../../test/energy/int_v18_m14_a1/planner_param.yaml ../../result/energy/int_v18_m14_a1/result_ccp.yaml
# Before executing the next line, change the flagSolver: TEAMPLANNER_DET to TEAMPLANNER_SPR in ../test/energy/int_v18_m14_a1/planner_param.yaml
./main ../../test/energy/int_v18_m14_a1/planner_param.yaml ../../result/energy/int_v18_m14_a1/result_spr.yaml
```

 The details of the three methods can be find in this \[[1](https://arxiv.org/pdf/2010.11376.pdf)\].

| Planner Modes | Descriptions  |
| ------------- |:-------------:|
|TEAMPLANNER_DET| The mean value is used as the traveling energy cost. |
|TEAMPLANNER_SPR| The uncertainty in the traveling energy cost is considered. The method is constrained optimization with resource.|
|TEAMPLANNER_CCP| The uncertainty in the traveling energy cost is considered. The method is chance constrained programming.|

### Plot the trajectories

After generating the results, use the plotting scripts in `offroadcase_traj.m` to plot the trajectories of the agents.


## Run the teaming case with uncertainties in the agent capabilities and task requirements

A task allocation problem: robotic services during a pandemic \[[2](https://arxiv.org/pdf/2106.12111.pdf)\].

<img src="readme/medical_case.png" alt="medical_case" width="50%"/>

```matlab
medical_g2o         % Preprocess the map information and generate the edges and node poses
medical_gpr_wrap    % Generate the edge costs based on a Gaussian process regression 
medical_case_gen    % Generate a test case consists of 16 tasks, 7 agent species, and 9 types of capabilities 
```

The optimization problem will be written to the files in the folder `../test/medical/con_v21_m16_a1/`.

The next step is to use the executable the above command in the directory of `../resilient_team_planner/build/` to find a teaming plan for the optimization problem.

The planning results will be output to the Linux command window as well as saved in `../result/medical/con_v21_m16_a1/result.yaml`.

### Multiple planning modes

Three planning modes have been run on this test case example.

```bash
./main ../../test/medical/int_v21_m16_a1/planner_param.yaml ../../result/medical/int_v21_m16_a1/result.yaml # TEAMPLANNER_DET
./main ../../test/medical/con_v21_m16_a1/planner_param.yaml ../../result/medical/con_v21_m16_a1/result.yaml # TEAMPLANNER_CONDET
# Before executing the next line, change the flagSolver: TEAMPLANNER_CONDET to TEAMPLANNER_CONTASKLSHAPED in ../test/medical/con_v21_m16_a1/planner_param.yaml
./main ../../test/medical/con_v21_m16_a1/planner_param.yaml ../../result/medical/con_v21_m16_a1/result_risk.yaml # TEAMPLANNER_CONTASKLSHAPED
```

The details of the three methods can be find in Table XI in \[[2](https://arxiv.org/pdf/2106.12111.pdf)\].

| Planner Modes             | Descriptions  |
| --------------------------|:-------------:|
|TEAMPLANNER_DET            | The method 'CTAS-O' in [2]. The task and capability uncertainty is not considered. |
|TEAMPLANNER_CONDET         | The method 'CTAS-D' in [2]. The task and capability uncertainty is not considered. Runs faster. |
|TEAMPLANNER_CONTASKLSHAPED | The method 'CTAS'   in [2]. The task and capability uncertainty is considered.|

### Plot the trajectories

After generating the results, use the plotting scripts in `medical_traj.m` to plot the trajectories of the agents.


## Attribution
YAMLMatlab 0.4.3: from https://code.google.com/archive/p/yamlmatlab/downloads

M3500 data set: from https://lucacarlone.mit.edu/datasets/ and https://github.com/RainerKuemmerle/g2o/wiki/File-Format-SLAM-2D

## References

[1] B. Fu, W. Smith, D. Rizzo, M. Castanier, and K. Barton, “Heterogeneous vehicle routing and teaming with Gaussian distributed energy uncertainty,” in 2020 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS). IEEE, 2020, pp. 4315-4322 \[[PDF](https://arxiv.org/pdf/2010.11376.pdf)\]

[2] B. Fu, W. Smith, D. Rizzo, M. Castanier, M. Ghaffari, and K. Barton, “Robust task scheduling for heterogeneous robot teams under capability uncertainty,” arXiv preprint arXiv:2106.12111, 2021. \[[PDF](https://arxiv.org/pdf/2106.12111.pdf)\] \[[Video](https://youtu.be/DE1DMnGHwwI)\]
