function [path_node, path_edge, path_dist, path_cost, path_cost_var] = my_getpath(G, start, goal, edge_id, edge_dist, edge_unit_eng_cov)

[path_node, path_cost] = shortestpath(G, start, goal, 'Method', 'auto');
path_node = path_node.';
% TR = shortestpathtree(G, start, goal);

len_path = length(path_node)-1;
path_edge = zeros(len_path, 1);
for i_edge = 1:(length(path_node)-1)
    path_edge(i_edge) =  edge_id(path_node(i_edge), path_node(i_edge+1));
end

path_cost_var = edge_dist(path_edge)' * edge_unit_eng_cov(path_edge, path_edge) * edge_dist(path_edge);
path_dist = sum(edge_dist(path_edge));


end

