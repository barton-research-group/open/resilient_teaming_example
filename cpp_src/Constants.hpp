#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#ifndef DO_DEBUG
#define DO_DEBUG true
#endif

#ifndef VALUE_TYPE
#define VALUE_TYPE double
#endif

#ifndef MyHeapType
#define MyHeapType MyHeapDouble
#endif

#ifndef HeapNodeType
#define HeapNodeType HeapNodeDouble
#endif

#ifndef DIRNUM
#define DIRNUM 8 // 8-connected grid
#endif

#endif