#ifndef PATHPLANNER_HPP
#define PATHPLANNER_HPP

#include <vector>
#include <unordered_map>
#include <chrono>
#include <math.h>
#include "State.hpp"

class PathPlanner
{
public:
	int x_size_;
	int y_size_;
	// std::unordered_map<int, State> state_;
	// std::vector<State> state_;
	State* state_;
    std::chrono::system_clock::time_point start_;
	double prev_time_;
	
public:
	PathPlanner();
	~PathPlanner();
	void Set(int x_size, int y_size);
	bool AStar(const double* map, double map_thres, const int* start_loc, const int* goal_loc, int flag_heuristics, double weight, int& closed_num, int& open_num, double& path_cost);
    bool GetPlan(const int* start_loc, const int* goal_loc, std::vector<std::vector<int>>& path);
    bool Plan(const double* map, double map_thres, const int* start_loc, const int* goal_loc, int flag_heuristics, double weight, std::vector<std::vector<int>>& path, double& path_cost);
	double getTime();
	double getDuration();
	void markTime();

	inline int sub2index(int i, int j){
		return ( i * y_size_ + j );
	}

	inline void index2sub(int index, int& x, int& y){
		x = index / y_size_;
		y = index - y_size_ * x;
	}

	template<class T>
	inline T heristics(T x1, T y1, T x2, T y2) {
		T dx = x1 - x2;
		T dy = y1 - y2;
		return ( (T) sqrt(dx*dx + dy*dy) );
	}

	// inline bool hasid(int id){ return ( state_.find(id) != state_.end() ); }

	inline bool isinside(int x, int y){ return ( x >= 0 && x < x_size_ && y >= 0 && y < y_size_ ); }
};

#endif