#include "PathPlanner.hpp"
#include "State.hpp"
#include "MyHeap.hpp"
#include "Constants.hpp"

// 8-connected grid
#define SQRT2 1.41421356237310
int dX_dir[DIRNUM] = {-1, -1, -1,  0,  0,  1, 1, 1};
int dY_dir[DIRNUM] = {-1,  0,  1, -1,  1, -1, 0, 1};
VALUE_TYPE cost_dir[DIRNUM] = {(VALUE_TYPE) SQRT2, (VALUE_TYPE) 1.0, (VALUE_TYPE) SQRT2, (VALUE_TYPE) 1.0, (VALUE_TYPE) 1.0, (VALUE_TYPE) SQRT2, (VALUE_TYPE) 1.0, (VALUE_TYPE) SQRT2};

PathPlanner::PathPlanner(){
	Set(0, 0);
}

PathPlanner::~PathPlanner(){}

void PathPlanner::Set(int x_size, int y_size){
	x_size_ = x_size;
	y_size_ = y_size;
	// start_ = std::chrono::system_clock::now();
	// markTime();
	// state_.clear();
}

double PathPlanner::getTime(){
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	int passed_duration = (int) std::chrono::duration_cast <std::chrono::milliseconds> (now-start_).count();
	return (( (double)passed_duration ) / 1000.0);
}

double PathPlanner::getDuration(){
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	int passed_duration = (int) std::chrono::duration_cast <std::chrono::milliseconds> (now-start_).count();
	return (( (double)passed_duration ) / 1000.0 - prev_time_);
}

void PathPlanner::markTime(){
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	int passed_duration = (int) std::chrono::duration_cast <std::chrono::milliseconds> (now-start_).count();
	prev_time_ = (( (double)passed_duration ) / 1000.0);
}

bool PathPlanner::AStar(const double* map, double map_thres, const int* start_loc, const int* goal_loc, int flag_heuristics, double weight, int& closed_num, int& open_num, double& path_cost) {
	double initialization_time = 0.0;
	double search_time = 0.0;
	double heap_time = 0.0;
	if(DO_DEBUG) {
		// initialization_time = getTime();
	}

	bool flag_break = false;
	closed_num = 0;
	open_num = 1;
	// state_.clear();
    path_cost = -1;

	VALUE_TYPE weight_inside = (VALUE_TYPE) weight;
	VALUE_TYPE goal_loc_inside_x = (VALUE_TYPE) goal_loc[0];
	VALUE_TYPE goal_loc_inside_y = (VALUE_TYPE) goal_loc[1];

	// state_.reserve(x_size_ * y_size_);
	state_ = new State[x_size_ * y_size_];

	// Initialize
    int goal_id = sub2index(goal_loc[0], goal_loc[1]);
    int start_id = sub2index(start_loc[0], start_loc[1]);
    int curr_x = start_loc[0];
    int curr_y = start_loc[1];
    int curr_id = start_id;
    int next_x, next_y, next_id;

	if( !isinside(start_loc[0], start_loc[1]) || (map[start_id] >= map_thres) ) {
        if (DO_DEBUG) {
    		printf("Invalid start location!\n");
        }
		return flag_break;
	}

	if( !isinside(goal_loc[0], goal_loc[1]) || (map[goal_id] >= map_thres) ) {
        if (DO_DEBUG) {
    		printf("Invalid goal location!\n");
        }
		return flag_break;
	}

	MyHeapType* theheap = new MyHeapType(2097152);

	// if(!hasid(curr_id)) {
		state_[curr_id] = State(curr_id);
	// }
	state_[curr_id].g_ = 0;
	HeapNodeType* curr_node = new HeapNodeType(curr_id, state_[curr_id].g_ + weight_inside * state_[curr_id].h_);
	HeapNodeType* next_node = nullptr;

	// if(DO_DEBUG) {
		// initialization_time = getTime() - initialization_time;
		// search_time = getTime();
	// }
	double other_time1 = 0.0;
	double other_time2 = 0.0;

	// Search
	while(curr_node != nullptr) {
		// if(DO_DEBUG) { markTime(); }
		curr_id = curr_node->id_;
        index2sub(curr_id, curr_x, curr_y);

		state_[curr_id].flag_closed_ = true;
		if(DO_DEBUG) {
			closed_num++;
			open_num--;
		}

		// std::vector<MyPoint3i> succ_pt_list;
		// std::vector<int> succ_normal_id_list;
		// std::vector<float> succ_cost_list;
		// GetSuccessors(map, curr_pt, curr_normal_id, succ_pt_list, succ_normal_id_list, succ_cost_list);
		// for(int i = 0; i < succ_pt_list.size(); i++){ // 6, 18, 26
		// 	next_pt = succ_pt_list[i];
		// 	next_normal_id = succ_normal_id_list[i];
		// 	next_id = sub2index(next_pt);
		// 	next_key = sub2key(next_pt, next_normal_id);
		// if(DO_DEBUG) { other_time1 += getDuration(); }

        for(int i = 0; i < DIRNUM; i++) {
			// if(DO_DEBUG) { markTime(); }
            next_x = curr_x + dX_dir[i];
            next_y = curr_y + dY_dir[i];
            next_id = sub2index(next_x, next_y);

            if( !isinside(next_x, next_y) || (map[next_id] >= map_thres) ){
				continue;
			}
			// if(!hasid(next_id)){ // state is unseen
			// 	state_[next_id] = State(next_id);
			// }
			// else 
			if(state_[next_id].flag_closed_){ // state is in closed
				continue;
			}

			// if(DO_DEBUG) { other_time2 += getDuration(); }
			VALUE_TYPE new_g = state_[curr_id].g_ + ( (VALUE_TYPE) (map[curr_id] + map[next_id]) ) * cost_dir[i] * 0.5;
			// if(DO_DEBUG) { markTime(); }
			if( new_g < state_[next_id].g_ ){
				state_[next_id].g_ = new_g;
				state_[next_id].parent_ = curr_id;
				if(theheap->hasId(next_id)){ // already in open
					theheap->update(next_id, new_g + state_[next_id].h_ );
				}
				else{
                    // Todo: compute h_ here
					state_[next_id].h_ = weight_inside * heristics( (VALUE_TYPE) next_x, (VALUE_TYPE) next_y, goal_loc_inside_x, goal_loc_inside_y );
					next_node = new HeapNodeType(next_id, new_g + state_[next_id].h_ );
					theheap->insert(next_node);
				    if(DO_DEBUG) {
	                    open_num++;
					}
				}
			}
			// if(DO_DEBUG) { heap_time += getDuration(); }
		}
		delete curr_node;
		if(curr_id == goal_id){
			flag_break = true;
            path_cost = (double) state_[curr_id].g_;
			break;
		}
		curr_node = theheap->deleteMin();
	}
    if(DO_DEBUG) {
        printf("Closed: %d, Open: %d, %d\n", closed_num, open_num, theheap->size() );
    }
	// if(DO_DEBUG) {
		// search_time = getTime() - search_time;
	// }
	// if(DO_DEBUG) {
	// 	markTime();
	// }
	delete theheap;
	delete[] state_;
	// if(DO_DEBUG) {
	// 	printf("delete_time = %f\n", getDuration());
	// }

	// if(DO_DEBUG) {
		// printf("initialization_time = %f, search_time = %f, other_time1 = %f, other_time2 = %f, heap_time = %f\n", initialization_time, search_time, other_time1, other_time2, heap_time);
	// }

	return flag_break;
}

bool PathPlanner::GetPlan(const int* start_loc, const int* goal_loc, std::vector<std::vector<int>>& path){
	path.clear();
	std::vector<int> path_id;

	int curr_id = sub2index(goal_loc[0], goal_loc[1]);
    int curr_x = -1;
    int curr_y = -1;

	while(curr_id != -1){
		path_id.push_back(curr_id);
		curr_id = state_[curr_id].parent_;
	}
	for(int i = (int) path_id.size() - 1; i >= 0; i--){
		curr_id = path_id[i];
		index2sub(curr_id, curr_x, curr_y);
        std::vector<int> curr_loc{curr_x, curr_y};
		path.push_back( curr_loc );
	}
	return true;
}

bool PathPlanner::Plan(const double* map, double map_thres, const int* start_loc, const int* goal_loc, int flag_heuristics, double weight, std::vector<std::vector<int>>& path, double& path_cost) {
	int closed_num = 0;
	int open_num = 0;
    bool flag_plan = AStar(map, map_thres, start_loc, goal_loc, flag_heuristics, weight, closed_num, open_num, path_cost);
	if(flag_plan){
        GetPlan(start_loc, goal_loc, path);
		return true;
	}
	else{
		return false;
	}
}
