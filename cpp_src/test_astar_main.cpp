#include <ctime>
#include <stdio.h>
#include "PathPlanner.hpp"

#include "PathPlanner.cpp"

int main(void)
{
    double map[] = {1, 3, 1,
    1, 3, 1,
    1, 3, 1,
    1, 3, 1,
    1, 1, 1
    };

    // Get map_thres
    double map_thres = 2.0;

    // Get start location
    double start_loc_double[] = {1, 1};
    int start_loc[2];
    start_loc[0] = (int) (start_loc_double[0] + 0.5) - 1;
    start_loc[1] = (int) (start_loc_double[1] + 0.5) - 1;
    printf("start_loc_double = %.1f, %.1f\n", start_loc_double[0], start_loc_double[1]);
    printf("start_loc = %d, %d\n", start_loc[0], start_loc[1]);

    // Get goal location
    double goal_loc_double[] = {1, 3};
    int goal_loc[2];
    goal_loc[0] = (int) (goal_loc_double[0] + 0.5) - 1;
    goal_loc[1] = (int) (goal_loc_double[1] + 0.5) - 1;
    printf("goal_loc_double = %.1f, %.1f\n", goal_loc_double[0], goal_loc_double[1]);
    printf("goal_loc = %d, %d\n", goal_loc[0], goal_loc[1]);

    // Call main function
    int flag_heuristics = 0;
    double weight = 1.0;
    std::vector<std::vector<int>> path;

    int map_x_size = 5;
    int map_y_size = 3;
    double path_cost = -1;

    PathPlanner path_planner = PathPlanner();
    path_planner.Set(map_x_size, map_y_size);
    bool flag_plan = path_planner.Plan(map, map_thres, start_loc, goal_loc, flag_heuristics, weight, path, path_cost);

    // Create output
    int path_size = 3;
    if(flag_plan) {
        path_size = (int) path.size();
    }
    double path_out[6];
    if(flag_plan) {
        for(int i = 0; i < (int) path.size(); i++) {
            printf("(%d, %d)\n", path[i][0], path[i][1]);
        }
    }
    else {
        path_out[0] = 10.0; path_out[1] = 11.0; path_out[2] = 12.0;
        path_out[3] = 13.0; path_out[4] = 14.0; path_out[5] = 15.0;
    }

    double path_cost_out[1];
    if(flag_plan) {
        *path_cost_out = 50.0;
    }
    else{
        *path_cost_out = -1.0;
    }

    return 0;
}