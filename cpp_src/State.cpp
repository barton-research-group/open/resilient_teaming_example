#include "State.hpp"

State::State(){
	key_ = -1;
	g_ = 20000000;
	h_ = 0;
	parent_ = -1;
	flag_closed_ = false;
}

State::State(int key){
	key_ = key;
	g_ = 20000000;
	h_ = 0;
	parent_ = -1;
	flag_closed_ = false;
}

State::~State(){}
