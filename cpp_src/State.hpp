#ifndef STATE_HPP
#define STATE_HPP

// #include <eigen3/Eigen/Core>
// #include <eigen3/Eigen/Geometry>
#include "Constants.hpp"

class State{
public:
	int key_;
	VALUE_TYPE g_;
	VALUE_TYPE h_;
	int parent_;
	bool flag_closed_;
	State();
	State(int key);
	~State();
};

#endif