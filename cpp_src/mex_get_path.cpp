#include <ctime>
#include <mex.h>
#include <stdio.h>
#include "PathPlanner.hpp"

#include "PathPlanner.cpp"

/* Input Arguments */
#define MAP_IN                      prhs[0]
#define MAP_THRES_IN                prhs[1]
#define START_LOC_IN                prhs[2]
#define GOAL_LOC_IN                 prhs[3]
#define WEIGHT_IN                   prhs[4]
#define HEURISTICS_IN               prhs[5]

/* Output Arguments */
#define PATH_OUT                    plhs[0]
#define PATH_COST_OUT               plhs[1]

void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray*prhs[] )
{
    
    /* Check for proper number of arguments */
    if (nrhs != 6) {
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidNumInputs", "6 input arguments required.");
    } else if (nlhs != 2) {
        mexErrMsgIdAndTxt( "MATLAB:planner:maxlhs", "2 output arguments required.");
    }

    // Get map
    // mwSize mxGetNumberOfDimensions(MAP_IN)
    // const mwSize* dims = mxGetDimensions(MAP_IN);
    int map_y_size = (int) mxGetM(MAP_IN); // Column number of the map
    int map_x_size = (int) mxGetN(MAP_IN); // Row number of the map
    double* map = mxGetPr(MAP_IN);
    if (DO_DEBUG) {
        printf("map_x_size, map_y_size = %d, %d\n", map_x_size, map_y_size);
    }
    // Get map_thres
    double map_thres = (double) mxGetScalar(MAP_THRES_IN);

    // Get start location
    double* start_loc_double = mxGetPr(START_LOC_IN);
    int start_loc[2];
    start_loc[0] = (int) (start_loc_double[0] + 0.5) - 1;
    start_loc[1] = (int) (start_loc_double[1] + 0.5) - 1;
    if (DO_DEBUG) {
        printf("start_loc_double = %.1f, %.1f\n", start_loc_double[0], start_loc_double[1]);
        printf("start_loc = %d, %d\n", start_loc[0], start_loc[1]);
    }

    // Get goal location
    double* goal_loc_double = mxGetPr(GOAL_LOC_IN);
    int goal_loc[2];
    goal_loc[0] = (int) (goal_loc_double[0] + 0.5) - 1;
    goal_loc[1] = (int) (goal_loc_double[1] + 0.5) - 1;
    if (DO_DEBUG) {
        printf("goal_loc_double = %.1f, %.1f\n", goal_loc_double[0], goal_loc_double[1]);
        printf("goal_loc = %d, %d\n", goal_loc[0], goal_loc[1]);
    }

    double* weight_double = mxGetPr(WEIGHT_IN);
    double weight = weight_double[0];

    double* flag_heuristics_double = mxGetPr(HEURISTICS_IN);
    int flag_heuristics = (int) (flag_heuristics_double[0] + 0.5);

    if (DO_DEBUG) {
        printf("weight, flag_heuristics = %f, %d\n", weight, flag_heuristics);
    }

    // Call main function
    std::vector<std::vector<int>> path;
    double path_cost = -1;

    PathPlanner path_planner = PathPlanner();
    path_planner.Set(map_x_size, map_y_size);
    bool flag_plan = path_planner.Plan(map, map_thres, start_loc, goal_loc, flag_heuristics, weight, path, path_cost);

    // Create output
    int path_size = 3;
    if(flag_plan) {
        path_size = (int) path.size();
    }
    PATH_OUT = mxCreateDoubleMatrix((mwSize) path_size, (mwSize) 2, mxREAL); // Create MATLAB array of same size
    double* path_out = mxGetPr(PATH_OUT);
    if(flag_plan) {
        for(int i = 0; i < path_size; i++) {
            path_out[i] = path[i][0] + 1;
            path_out[i + path_size] = path[i][1] + 1;
        }
    }
    else {
        path_out[0] = 10.0; path_out[1] = 11.0; path_out[2] = 12.0;
        path_out[3] = 13.0; path_out[4] = 14.0; path_out[5] = 15.0;
    }

    PATH_COST_OUT = mxCreateDoubleMatrix((mwSize) 1, (mwSize) 1, mxREAL); // Create MATLAB array of same size
    double* path_cost_out = mxGetPr(PATH_COST_OUT);
    if(flag_plan) {
        *path_cost_out = path_cost;
    }
    else{
        *path_cost_out = -1.0;
    }

    // P_TRUE_OUT = mxCreateDoubleMatrix((mwSize)P_true.rows(), (mwSize)P_true.cols(), mxREAL); // Create MATLAB array of same size
    // Eigen::Map<Eigen::MatrixXd> p_true_matlab(mxGetPr(P_TRUE_OUT), (mwSize)P_true.rows(), (mwSize)P_true.cols()); // Map the array
    // p_true_matlab = P_true; // Copy
    
    // P_NFR_OUT = mxCreateDoubleMatrix((mwSize)P_nfr.rows(), (mwSize)P_nfr.cols(), mxREAL); // Create MATLAB array of same size
    // Eigen::Map<Eigen::MatrixXd> p_nfr_matlab(mxGetPr(P_NFR_OUT), (mwSize)P_nfr.rows(), (mwSize)P_nfr.cols()); // Map the array
    // p_nfr_matlab = P_nfr; // Copy

    return;   
}