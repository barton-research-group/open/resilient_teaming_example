classdef World
    %The class that contains map information and can plan upon it
    %   Detailed explanation goes here
    
    properties
        eng_map = []
        eng_map_var = []
        eng_map_cov = []
        obs_map = []
        x_size = 1
        y_size = 1
        map_res = 1
        eng_map_thres = 1
        obs_map_thres = 1
        weight = 1
        flag_heuristics = 1
    end
    
    %% Methods
    methods
        function obj = World(obs_map, eng_map, eng_map_var, map_res, eng_map_thres, obs_map_thres, weight, flag_heuristics, eng_map_cov)
            %Construct an instance
            obj.y_size = size(obs_map, 1);
            obj.x_size = size(obs_map, 2);
            if obj.x_size ~= size(eng_map, 2) || ...
                    obj.x_size ~= size(eng_map_var, 2) || ...
                    obj.y_size ~= size(eng_map, 1) || ...
                    obj.y_size ~= size(eng_map_var, 1)
                error('Map size inconsistent.')
            end
            obj.map_res = map_res;
            obj.obs_map = obs_map;
            obj.eng_map = eng_map;
            obj.eng_map_var = eng_map_var;
            obj.eng_map_thres = eng_map_thres;
            obj.obs_map_thres = obs_map_thres;
            obj.weight = weight;
            obj.flag_heuristics = flag_heuristics;
            if nargin >= 9
                obj.eng_map_cov = eng_map_cov;
            end
        end
        
        function loc_int = double2int(obj, loc)
            loc_int = round(loc / obj.map_res);
        end
        
        function [path_traj_int, path_cost, path_sigma] = get_path(obj, start_loc, goal_loc)
            start_loc_int = obj.double2int(start_loc);
            goal_loc_int = obj.double2int(goal_loc);
            [path_traj_int, path_cost_int] = mex_get_path(obj.obs_map, obj.obs_map_thres, start_loc_int, goal_loc_int, obj.weight, obj.flag_heuristics);
            path_cost = path_cost_int * obj.map_res;
            path_sigma = 1; % TODO
        end

        function [path_traj_int, path_cost, path_var1, path_var2] = get_eng_path(obj, start_loc, goal_loc, gp_regressor)
            start_loc_int = obj.double2int(start_loc);
            goal_loc_int = obj.double2int(goal_loc);
            [path_traj_int, path_cost_int] = mex_get_path(obj.eng_map, obj.eng_map_thres, start_loc_int, goal_loc_int, obj.weight, obj.flag_heuristics);
            path_cost = path_cost_int * obj.map_res;
            path_var1 = -1; % TODO
            path_var2 = -1;
            if nargout >= 3
                [~, path_var1] = obj.get_uncertainty_from_gp(gp_regressor, path_traj_int);
                [~, path_var2] = obj.get_uncertainty_from_map(path_traj_int);
            end
        end

        function [path_mu, path_var] = get_uncertainty_from_gp(obj, gp_regressor, path_traj_int)
            path_traj = path_traj_int * obj.map_res;
            [path_traj_mu, path_traj_cov] = gp_regressor.predict(path_traj);
            path_traj_diff = sqrt( sum( diff(path_traj, 1, 1).^2, 2) ); % diff once along row dimension
            path_traj_dist = ([0; path_traj_diff] + [path_traj_diff; 0]) / 2;
            path_mu = path_traj_mu.' * path_traj_dist;
            path_var = sum( sum( (path_traj_dist * path_traj_dist.') .* path_traj_cov ) );
            % path_var = path_traj_dist.'  * path_traj_cov * path_traj_dist;
        end
        
        function [path_mu, path_var] = get_uncertainty_from_map(obj, path_traj_int)
            % This is an approximation of the path cov, because only the
            % diagnal entries in the cov matrices are considered
            path_traj = path_traj_int * obj.map_res;
            path_traj_diff = sqrt( sum( diff(path_traj, 1, 1).^2, 2) ); % diff once along row dimension
            path_traj_dist = ([0; path_traj_diff] + [path_traj_diff; 0]) / 2;
            index_path = sub2ind([obj.y_size, obj.x_size], path_traj_int(:,2), path_traj_int(:,1) );
            if size(index_path, 2) > 1
                index_path = index_path(:);
            end
            path_traj_mu = obj.eng_map(index_path);
            path_mu = path_traj_mu.' * path_traj_dist;
            if isempty(obj.eng_map_cov)
                path_traj_cov_diag = obj.eng_map_var(index_path);
                path_var = path_traj_cov_diag.' * (path_traj_dist.^2);
            else
                path_traj_cov = obj.eng_map_cov(index_path, index_path);
                path_var = path_traj_dist.' * path_traj_cov * path_traj_dist;
            end
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
        
    end
end

