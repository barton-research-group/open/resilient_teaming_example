clc;
clear;

load('data/M3500.mat'); % Load pose_data, edge_data

n_pose = size(pose_data, 1);
n_edge = size(edge_data, 1);
edge_cost = edge_dist;

n_sample = 6;

sample_list1 = [304;952;744;329;460;879];   % Randomly select some poeses to be with high traveling cost (risky)
sample_pose1 = pose_data(sample_list1, :);
sample_cost1 = ones(n_sample, 1) * 100;

sample_list2 = [213;392;198;614;385;409];   % Randomly select some poeses to be with low traveling cost (safe)
sample_pose2 = pose_data(sample_list2, :);
sample_cost2 = ones(n_sample, 1) * 10;

sample_list = [sample_list1; sample_list2];
sample_pose = [sample_pose1; sample_pose2];
sample_cost = [sample_cost1; sample_cost2];

font_size = 20;
font_name = 'Times New Roman';
line_width = 1.5;

% Run medical_gpr or load the results
medical_gpr; % The outputs are saved to 'data/M3500_gpr.mat'
% load('data/M3500_gpr.mat');

%% Show map
figure(1);
thecolor = get(gca, 'colororder');

plot(pose_data(:,1), pose_data(:,2), 'k.');
hold on;
% plot(edge_x.', edge_y.', 'Color', [1, 1, 1] * 0.7);
plot(sample_pose1(:,1), sample_pose1(:,2), '.', 'Color', thecolor(2, :), 'MarkerSize', 30);
plot(sample_pose2(:,1), sample_pose2(:,2), '.', 'Color', thecolor(5, :), 'MarkerSize', 30);
hold off;
axis equal;
title('Sample points');
legend('', 'high cost', 'low cost');
map_plot_resize;

%% Show Gaussian process map
figure(2);
eng_range = [10, 100];
imagesc(eng_map, eng_range); axis equal; colorbar;
median_range = [min(eng_map(:)), eng_map_prior_mean-eng_map_margin, eng_map_prior_mean+eng_map_margin, max(eng_map(:))];
color_map = my_colormap(1, median_range);
colormap(color_map);
hold on;
plot(pose_data(:,1), pose_data(:,2), 'k.');
hold off;
set(gca, 'ydir', 'normal'); % reverse, normal
title('Cost to travel');
map_plot_resize;

%% Save
flag_print = true;
if flag_print
    print_folder = ['./temp/'];
    for index_plot = 1:2
        thefigure = figure(index_plot);
        print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(300)]);
        % saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
end
