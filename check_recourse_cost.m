function [recourse_cost, index_ij_list] = check_recourse_cost(n_mission, n_vehicle, n_link, mission_sub2ind, index_vehicle, x, edge_cost, edge_var, vehicle_eng_cap_this)

% index_vehicle = 3;
% vehicle_eng_cap_this = vehicle_eng_cap(index_vehicle );

recourse_cost = 0;
index_vehicle1 = 1;

type = 1;
index_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, -1, -1);
x_this = x(index_x);

max_iter = n_mission + 2;


index_ij_list = zeros(n_mission + 2, 1);
index_ij_list(1) = 0;

%% Generate the path
for index_iter = 1:max_iter
    if index_iter == 1
        index_i = 0;
    end
    
    index_edge_list = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle1, index_i, -1);
    index_j = find(x_this(index_edge_list) > 0.5);
    if(isempty(index_j))
        if index_iter == 1
            recourse_cost = 0;
            index_ij_list = [];
        else
            recourse_cost = -1;
        end
        break;
    end
    if length(index_j) >= 2
        recourse_cost = -1;
        break;
    end
    
    if index_iter > 1
        index_j_list = mission_sub2ind(index_i, :);
        index_j_list = find(index_j_list > 0);
        n_i_link = length(index_j_list);
        if (index_j > n_i_link)
            index_ij_list(index_iter+1) = 0;
            break;
        else
            index_j = index_j_list(index_j);
        end
    end
    
    index_ij_list(index_iter+1) = index_j;
    index_i = index_j;
    
end

if recourse_cost == -1
    index_ij_list = [];
else
    if (~isempty(index_ij_list))
        index_ij_list( (index_iter+2):end) = [];
    end
end

if(isempty(index_ij_list))
    %     return;
end

%% Calculate the path cost and variance
n_edge_this = length(index_ij_list) - 1;
index_edge_list = zeros(n_edge_this,1);
for index_edge = 1: n_edge_this
    index_i = index_ij_list(index_edge);
    index_j = index_ij_list(index_edge+1);
    index_edge_list(index_edge) = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, index_i, index_j);
end

edge_cost_this = edge_cost(index_edge_list);
edge_var_this = edge_var(index_edge_list);

edge_cost_sum_this = cumsum(edge_cost_this);
edge_var_sum_this = cumsum(edge_var_this);
edge_sigma_sum_this = sqrt(edge_var_sum_this);

%% Calculate recourse cost for each case
recourse_cost_list = zeros(n_edge_this,1);
recourse_cost_prev = 0;

for index_edge = 1: n_edge_this
    % index_i = index_ij_list(index_edge);
    index_j = index_ij_list(index_edge+1);
    if index_j ~= 0
        index_edge_from_start = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle1, 0, index_j);
        index_edge_to_goal = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle1, index_j, 0);
        index_edge_replace = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, 0, index_j);
        
        recourse_cost_curr = edge_cost(index_edge_from_start) + edge_cost(index_edge_to_goal);
        recourse_cost_list(index_edge) = (recourse_cost_curr + recourse_cost_prev) / 2 + edge_cost(index_edge_replace);
        recourse_cost_prev = recourse_cost_curr;
    else
        recourse_cost_list(index_edge) = recourse_cost_prev / 2;
    end
end



%% Calculate total recourse cost
recourse_cost = 0;
for index_fail = 1:3
    prob_larger = normcdf( (vehicle_eng_cap_this * index_fail - edge_cost_sum_this ) ./ (edge_sigma_sum_this + 1e-6) );
    prob_smaller = [1; prob_larger(1:end-1)];
    prob_fail = prob_smaller - prob_larger;
    recourse_cost = recourse_cost + sum(prob_fail .* recourse_cost_list);
end



end