
base_pose = pose_data(base_node, :);
start_node_list = ones(n_vehicle, 1) * base_node;
start_loc_list = zeros(n_vehicle, 2) + base_pose;
terminal_node_list = start_node_list;
terminal_loc_list = start_loc_list;

mission_node_list = randperm(n_pose, n_mission).';
mission_loc_int_list = pose_data(mission_node_list, :);

mission_loc_list = mission_loc_int_list;
start_loc_int_list = start_loc_list;
terminal_loc_int_list = terminal_loc_list;

for index_vehicle = 1:n_vehicle
    for index_mission = 1:n_mission
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, 0, index_mission);
        start_node = start_node_list(index_vehicle, :);
        goal_node = mission_node_list(index_mission, :);
        start_loc = start_loc_list(index_vehicle, :);
        goal_loc = mission_loc_list(index_mission, :);
        if index_vehicle <= n_av
            [path_node, path_edge, path_dist, path_cost, path_cost_var] = my_getpath(G, start_node, goal_node, mapedge_id, edge_dist, edge_unit_eng_cov);
            fpath{ind_x} = pose_data(path_node, :);
            edge_cost(ind_x) = path_cost * vehicle_cost(index_vehicle);
            edge_var(ind_x) = path_cost_var * vehicle_cost(index_vehicle)^2;
            if flag_edge_time_cost
                edge_time_cost(ind_x) = path_dist / vehicle_vel;
            end
        else
            idx_x_use = mod(ind_x-1, n_repeat)+1;
            fpath{ind_x} = fpath{idx_x_use};
            edge_cost(ind_x) = edge_cost(idx_x_use);
            edge_var(ind_x) = edge_var(idx_x_use);
            edge_time_cost(ind_x) = edge_time_cost(idx_x_use);
        end
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = index_vehicle + n_mission;
        edge_j(ind_x) = index_mission;
        %         if ~flag_draw_path
        %             fpath{ind_x} = [start_loc; goal_loc];
        %         end
        %         if flag_dist_cost_list(index_vehicle)
        %             edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost * vehicle_cost(index_vehicle);
        %             edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2 * vehicle_cost(index_vehicle)^2;
        %         end
    end
    for index_mission = 1:n_mission
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, index_mission, 0);
        start_node = mission_node_list(index_mission, :);
        goal_node = terminal_node_list(index_vehicle, :);
        start_loc = mission_loc_list(index_mission, :);
        goal_loc = terminal_loc_list(index_vehicle, :);
        if index_vehicle <= n_av
            [path_node, path_edge, path_dist, path_cost, path_cost_var] = my_getpath(G, start_node, goal_node, mapedge_id, edge_dist, edge_unit_eng_cov);
            fpath{ind_x} = pose_data(path_node, :);
            edge_cost(ind_x) = path_cost * vehicle_cost(index_vehicle);
            edge_var(ind_x) = path_cost_var * vehicle_cost(index_vehicle)^2;
            if flag_edge_time_cost
                edge_time_cost(ind_x) = path_dist / vehicle_vel;
            end
        else
            idx_x_use = mod(ind_x-1, n_repeat)+1;
            fpath{ind_x} = fpath{idx_x_use};
            edge_cost(ind_x) = edge_cost(idx_x_use);
            edge_var(ind_x) = edge_var(idx_x_use);
            edge_time_cost(ind_x) = edge_time_cost(idx_x_use);
        end
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = index_mission;
        edge_j(ind_x) = index_vehicle + n_vehicle + n_mission;
        %         if ~flag_draw_path
        %             fpath{ind_x} = [start_loc.'; goal_loc.'];
        %         end
        %         if flag_dist_cost_list(index_vehicle)
        %             edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost;
        %             edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2;
        %         end
    end
    for index_link = 1:size(mission_ind2sub, 1)
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, mission_ind2sub(index_link, 1), mission_ind2sub(index_link, 2));
        start_node = mission_node_list(mission_ind2sub(index_link, 1), :);
        goal_node = mission_node_list(mission_ind2sub(index_link, 2), :);
        start_loc = mission_loc_list(mission_ind2sub(index_link, 1), :);
        goal_loc = mission_loc_list(mission_ind2sub(index_link, 2), :);
        if index_vehicle <= n_av
            [path_node, path_edge, path_dist, path_cost, path_cost_var] = my_getpath(G, start_node, goal_node, mapedge_id, edge_dist, edge_unit_eng_cov);
            fpath{ind_x} = pose_data(path_node, :);
            edge_cost(ind_x) = path_cost * vehicle_cost(index_vehicle);
            edge_var(ind_x) = path_cost_var * vehicle_cost(index_vehicle)^2;
            if flag_edge_time_cost
                edge_time_cost(ind_x) = path_dist / vehicle_vel;
            end
        else
            idx_x_use = mod(ind_x-1, n_repeat)+1;
            fpath{ind_x} = fpath{idx_x_use};
            edge_cost(ind_x) = edge_cost(idx_x_use);
            edge_var(ind_x) = edge_var(idx_x_use);
            edge_time_cost(ind_x) = edge_time_cost(idx_x_use);
        end
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = mission_ind2sub(index_link, 1);
        edge_j(ind_x) = mission_ind2sub(index_link, 2);
        %         if ~flag_draw_path
        %             fpath{ind_x} = [start_loc.'; goal_loc.'];
        %         end
        %         if flag_dist_cost_list(index_vehicle)
        %             edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost;
        %             edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2;
        %         end
    end
end
