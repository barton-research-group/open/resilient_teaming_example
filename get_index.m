function index_together = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, ...
                                    type, index_vehicle, index_from, index_to, n_cap)

% Arguments:
%     type            - scalar, 1: robot pose, 2: landmark pose, <=-1: global index
%     index_local     - scalar, the index for robot pose or landmark
%     n_offset        - scalar, n_offset = p_dim*n_poses
% Returns:
%     index_together  - 2x1 matrix, the indeces for robot pose or landmark

switch type
    case 1 % X
        if index_vehicle == -1
            % Not allowed
            error('index_vehicle == -1 not allowed.');
        else
            if index_from == -1
                if index_to == -1
                    index_together = ( 1 : (n_link + 2 * n_mission) ).';
                    % Not allowed
                    % error('index_from == -1 && index_to == -1 not allowed.');
                else
                    if index_to > 0 % All to mission
                        index_together_temp = mission_sub2ind(:, index_to);
                        index_together_temp = full(index_together_temp(index_together_temp>0));
                        index_together = [index_together_temp(:); n_link + index_to];
                    elseif index_to == 0 % All to end
                        index_together = (n_link + n_mission + 1 : n_link + 2 * n_mission).';
                    else
                        error('Wrong index_to!');
                    end
                end
            else
                if index_to == -1
                    if index_from > 0 % Mission to all
                        index_together_temp = mission_sub2ind(index_from, :);
                        index_together_temp = full(index_together_temp(index_together_temp>0));
                        index_together = [index_together_temp(:); n_link + n_mission + index_from];
                    elseif index_from == 0 % Start to all
                        index_together = (n_link + 1 : n_link + n_mission).';
                    else
                        error('Wrong index_from!');
                    end
                else
                    if index_from > 0 && index_to > 0 % Mission to mission
                        index_together = full(mission_sub2ind(index_from, index_to));
                        if index_together == 0
                            error('No map link from index_from to index_to!')
                        end
                    elseif index_from == 0 && index_to > 0 % Start to mission
                        index_together = n_link + index_to;
                    elseif index_from > 0 && index_to == 0 % Mission to end
                        index_together = n_link + n_mission + index_from;
                    else
                        error('Wrong index_from or index_to!');
                    end
                end
            end
        end
        index_together = (n_link + 2 * n_mission) * (index_vehicle - 1) + index_together;
    case 2 % Y
        if index_vehicle == -1
            if index_from == -1
                index_vehicle_temp = 1:n_vehicle;
                index_from_temp = 1:n_mission;
            else
                index_vehicle_temp = 1:n_vehicle;
                index_from_temp = index_from;
            end
        else
            if index_from == -1
                index_vehicle_temp = index_vehicle;
                index_from_temp = 1:n_mission;
            else
                index_vehicle_temp = index_vehicle;
                index_from_temp = index_from;
            end
        end
        n_offset = (n_link + 2*n_mission) * n_vehicle;
        [index_vehicle_temp, index_from_temp] = meshgrid(index_vehicle_temp, index_from_temp);
        index_together = n_offset + (index_vehicle_temp(:) - 1) * n_mission + index_from_temp(:);
    case 3 % Z
        if index_from == -1
            index_from_temp = (1:n_mission).';
        else
            index_from_temp = index_from;
        end
        n_offset = (n_link + 2*n_mission) * n_vehicle + n_mission * n_vehicle;
        index_together = n_offset + index_from_temp;
    case 4 % Q
        if index_from == -1
            index_from_temp = (1: (n_mission + 2 * n_vehicle) ).';
        else
            index_from_temp = index_from;
        end
        n_offset = (n_link + 2*n_mission) * n_vehicle + n_mission * n_vehicle + n_mission;
        index_together = n_offset + index_from_temp;
    case 5 % P
        if index_from == -1
            error('Wrong index_from!');
        else
            index_together = (index_from - 1) * n_cap + index_to;
        end
        n_offset = (n_link + 2*n_mission) * n_vehicle + n_mission * n_vehicle + n_mission + (n_mission + 2 * n_vehicle);
        index_together = n_offset + index_together;
    case 6 % alpha
        if index_from == -1
            error('Wrong index_from!');
        else
            index_together = (index_from - 1) * n_cap + index_to;
        end
        n_offset = (n_link + 2*n_mission) * n_vehicle + n_mission * n_vehicle + n_mission + (n_mission + 2 * n_vehicle) + n_mission * n_cap;
        index_together = n_offset + index_together;
    otherwise
        index_together = -1;
end

end
