% rng(1000);

map_y_size = map_size(1) * map_res;
map_x_size = map_size(2) * map_res;

start_loc_list = zeros(n_vehicle, 2) * 20 + [map_x_size / 2 - 10, map_y_size / 2 - 10];
terminal_loc_list = start_loc_list;
% mission_locx_list = rand(n_mission, 1) * map_x_size;
% mission_locy_list = rand(n_mission, 1) * map_y_size;
% mission_loc_list = [mission_locx_list, mission_locy_list];


mission_locind_int_list = randperm(prod(map_size), n_mission*3).';
mission_locind_int_list = mission_locind_int_list(gt_map(mission_locind_int_list) < gt_map_thres);
mission_locind_int_list = mission_locind_int_list(1:n_mission);
[mission_locy_int_list, mission_locx_int_list] = ind2sub(map_size, mission_locind_int_list);
mission_loc_int_list = [mission_locx_int_list, mission_locy_int_list];
mission_loc_list = mission_loc_int_list * map_res;

start_loc_int_list = start_loc_list / map_res;
terminal_loc_int_list = terminal_loc_list / map_res;

for index_vehicle = 1:n_vehicle
    for index_mission = 1:n_mission
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, 0, index_mission);
        start_loc = start_loc_list(index_vehicle, :).';
        goal_loc = mission_loc_list(index_mission, :).';
        %             [fpath{ind_x}, fobj(ind_x)] = mex_get_path(obs_map, obs_map_thres, start_loc, goal_loc, weight, flag_heuristics);
        %             [fpath{ind_x}, fobj(ind_x), ~] = world_obj.get_path(start_loc, goal_loc);
        if index_vehicle <= n_av
            [fpath{ind_x}, edge_cost(ind_x), edge_var(ind_x)] = world_obj.get_eng_path(start_loc, goal_loc, gpr);
            edge_cost(ind_x) = edge_cost(ind_x) * vehicle_cost(index_vehicle);
            edge_var(ind_x) = edge_var(ind_x) * vehicle_cost(index_vehicle)^2;
        else
            idx_x_use = mod(ind_x-1, n_repeat)+1;
            fpath{ind_x} = fpath{idx_x_use};
            edge_cost(ind_x) = edge_cost(idx_x_use);
            edge_var(ind_x) = edge_var(idx_x_use);
        end
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = index_vehicle + n_mission;
        edge_j(ind_x) = index_mission;
        if ~flag_draw_path
            fpath{ind_x} = [start_loc.'; goal_loc.'];
        end
        if flag_dist_cost_list(index_vehicle)
            edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost * vehicle_cost(index_vehicle);
            edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2 * vehicle_cost(index_vehicle)^2;
        end
    end
    for index_mission = 1:n_mission
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, index_mission, 0);
        start_loc = mission_loc_list(index_mission, :).';
        goal_loc = terminal_loc_list(index_vehicle, :).';
        %             [fpath{ind_x}, fobj(ind_x)] = mex_get_path(obs_map, obs_map_thres, start_loc, goal_loc, weight, flag_heuristics);
        %             [fpath{ind_x}, fobj(ind_x), ~] = world_obj.get_path(start_loc, goal_loc);
        if index_vehicle <= n_av
            [fpath{ind_x}, edge_cost(ind_x), edge_var(ind_x)] = world_obj.get_eng_path(start_loc, goal_loc, gpr);
            edge_cost(ind_x) = edge_cost(ind_x) * vehicle_cost(index_vehicle);
            edge_var(ind_x) = edge_var(ind_x) * vehicle_cost(index_vehicle)^2;
        else
            idx_x_use = mod(ind_x-1, n_repeat)+1;
            fpath{ind_x} = fpath{idx_x_use};
            edge_cost(ind_x) = edge_cost(idx_x_use);
            edge_var(ind_x) = edge_var(idx_x_use);
        end
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = index_mission;
        edge_j(ind_x) = index_vehicle + n_vehicle + n_mission;
        if ~flag_draw_path
            fpath{ind_x} = [start_loc.'; goal_loc.'];
        end
        if flag_dist_cost_list(index_vehicle)
            edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost;
            edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2;
        end
    end
    for index_link = 1:size(mission_ind2sub, 1)
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, mission_ind2sub(index_link, 1), mission_ind2sub(index_link, 2));
        start_loc = mission_loc_list(mission_ind2sub(index_link, 1), :).';
        goal_loc = mission_loc_list(mission_ind2sub(index_link, 2), :).';
        %             [fpath{ind_x}, fobj(ind_x)] = mex_get_path(obs_map, obs_map_thres, start_loc, goal_loc, weight, flag_heuristics);
        %             [fpath{ind_x}, fobj(ind_x), ~] = world_obj.get_path(start_loc, goal_loc);
        if index_vehicle <= n_av
            [fpath{ind_x}, edge_cost(ind_x), edge_var(ind_x)] = world_obj.get_eng_path(start_loc, goal_loc, gpr);
            edge_cost(ind_x) = edge_cost(ind_x) * vehicle_cost(index_vehicle);
            edge_var(ind_x) = edge_var(ind_x) * vehicle_cost(index_vehicle)^2;
        else
            idx_x_use = mod(ind_x-1, n_repeat)+1;
            fpath{ind_x} = fpath{idx_x_use};
            edge_cost(ind_x) = edge_cost(idx_x_use);
            edge_var(ind_x) = edge_var(idx_x_use);
        end
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = mission_ind2sub(index_link, 1);
        edge_j(ind_x) = mission_ind2sub(index_link, 2);
        if ~flag_draw_path
            fpath{ind_x} = [start_loc.'; goal_loc.'];
        end
        if flag_dist_cost_list(index_vehicle)
            edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost;
            edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2;
        end
    end
end
