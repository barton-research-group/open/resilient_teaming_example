%% Read results
addpath('lib/YAMLMatlab_0.4.3/');
round_result_file = [mat_result_folder, sub_folder, 'round.yaml'];
round_result = ReadYaml(round_result_file);
int_flow = cell2mat(round_result.intFlowVar);

% %% Plot
% set(0,'defaultAxesFontSize',20)

figure(1);
n_edge = length(int_flow);
edge_label = cell(n_edge, 1);

edge_flow_label = round(edge_flow * 100) / 100;

for i = 1:n_edge
    edge_label{i} = [num2str(edge_flow_label(i)), ' < ', num2str(int_flow(i))];
end
H2 = plot(GF,'EdgeLabel', edge_label, 'NodeLabel', node_code,'Layout','layered', 'LineWidth', 2, 'MarkerSize', 5, 'ArrowSize', 10);

% set(gca, 'FontName', 'Times New Roman')
set(gca, 'FontSize', 10)


%% Save
flag_print = true;
if flag_print
    %pause;
    print_folder = ['./temp/'];
    for index_plot = 1
        thefigure = figure(index_plot);
        print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(400)]);
%         print(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf'],'-dpdf');
%         saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
%     close all;
end