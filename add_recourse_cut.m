function [n_ieq_more, Aieq_i_more, Aieq_j_more, Aieq_v_more, bieq_more] = add_recourse_cut(n_no_recourse_var, n_mission, n_vehicle, n_link, mission_sub2ind, x, index_vehicle, recourse_cost, n_ieq)

type = 1;
index_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, -1, -1);

index_x_one = false(size(x));
index_x_one(index_x) = true;
index_x_one = index_x_one & (x > 0);
index_x_one = find(index_x_one);

index_x_one2 = index_x(x(index_x) > 0);

n_s = length(index_x_one);

n_ieq_more = n_ieq + 1;
Aieq_i_more = ones(n_s+1, 1) * n_ieq_more;
Aieq_j_more = [index_x_one; n_no_recourse_var + index_vehicle];
Aieq_v_more = [ones(n_s, 1) * recourse_cost; -1];
bieq_more = recourse_cost * (n_s-1);

end