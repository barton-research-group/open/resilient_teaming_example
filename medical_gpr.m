% close all;
% clear;
% clc;

flag_save_map_data = true;
flag_title = true;
flag_save_cov = true;
flag_print_cov = false;

seed = 2000;
rng(seed);

save_name = 'data/M3500_gpr.mat';

map_size = [60, 100];
map_res = 1;

eng_map_sigma_f = 6;
eng_map_sigma_white = 1;

edge_mean_pose = (edge_xy1 + edge_xy2) / 2;

%% Gaussian Process Registration
eng_map_lambda = 3;
eng_map_prior_mean = 30;
eng_map_margin = 5;

index_x = sample_pose(:, 1);
index_y = sample_pose(:, 2);

sample_loc = sample_pose;
sample_eng = sample_cost;

gpr = GPRegressor(); % Initialize a Gaussian process class
tic;
gpr.fit(sample_loc, sample_eng, eng_map_lambda, eng_map_prior_mean, eng_map_sigma_f, eng_map_sigma_white);
fit_time = toc;

[xx, yy] = meshgrid(1:map_size(2), 1:map_size(1));
index_valid = xx > -100;
eng_map = zeros(map_size);

eng_map_var = zeros(map_size);
tic;
[eng_map_temp, eng_map_cov_temp] = gpr.predict([xx(index_valid)*map_res, yy(index_valid)*map_res]);
map_predict_time = toc;
tic;
[edge_unit_eng, edge_unit_eng_cov] = gpr.predict([edge_mean_pose(:,1)*map_res, edge_mean_pose(:,2)*map_res]);
edge_predict_time = toc;
eng_map_var_temp = diag(eng_map_cov_temp);

eng_map(index_valid) = eng_map_temp;
eng_map_var(index_valid) = eng_map_var_temp;

%% Initialize map
eng_range = [-inf, inf];

if flag_save_map_data
    if flag_save_cov
        save(save_name, 'eng_map', 'eng_map_var', 'map_res', 'map_size', 'gpr', 'eng_map_prior_mean', 'eng_map_margin', 'edge_unit_eng', 'edge_unit_eng_cov');
    else
        save(save_name, 'eng_map', 'eng_map_var', 'map_res', 'map_size', 'gpr', 'eng_map_prior_mean', 'eng_map_margin', 'edge_unit_eng');
    end
end

%% Plots
font_size = 30;
font_name = 'Times New Roman';
line_width = 1.5;

if false
    % Predicted energy map
    figure(3);
    imagesc(eng_map, eng_range); axis equal; colorbar;
    median_range = [min(eng_map(:)), eng_map_prior_mean-eng_map_margin, eng_map_prior_mean+eng_map_margin, max(eng_map(:))];
    color_map = my_colormap(1, median_range);
    colormap(color_map);
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    %     colormap jet;
    set(gca, 'ydir', 'normal'); % reverse, normal
    if flag_title
        title('Predicted energy map');
    end
    map_plot_resize;
    
    figure(4);
    scatter(edge_mean_pose(:,1), edge_mean_pose(:,2), [], edge_unit_eng, 'Marker', '.');
    axis equal;
    color_map = my_colormap(1, median_range);
    colorbar; colormap(color_map);
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    if flag_title
        title('Predicted edge energy');
    end
    map_plot_resize;
    
    % Predicted energy map uncertainty
    figure(5);
    eng_map_sigma_plot = zeros(map_size);
    eng_map_sigma_plot(index_valid) = sqrt(eng_map_var(index_valid));
    color_map = my_colormap(0);
    imagesc(eng_map_sigma_plot); axis equal; colorbar; colormap(color_map);
    set(gca, 'ydir', 'normal'); % reverse, normal
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    if flag_title
        title('Predicted energy map uncertainty');
    end
    map_plot_resize;
end

if flag_print_cov
    % Covariance matrix
    figure(6);
    imshow(eng_map_cov_temp, []); axis equal; colorbar; colormap jet;
    if flag_title
        title('Covariance matrix');
    end
    % map_plot_resize;
    index_pixel_x_chosen_list = [30;11];
    index_pixel_y_chosen_list = [16;34];
    index_pixel_chosen_list = sub2ind(map_size, index_pixel_x_chosen_list, index_pixel_y_chosen_list);
    
    clims = [min(min(eng_map_cov_temp(:, index_pixel_chosen_list))), max(max(eng_map_cov_temp(:, index_pixel_chosen_list)))];
    % Cov matrix w.r.t pixel i
    for i_pixel = 1:length(index_pixel_x_chosen_list)
        index_pixel_x_chosen = index_pixel_x_chosen_list(i_pixel);
        index_pixel_y_chosen = index_pixel_y_chosen_list(i_pixel);
        index_pixel_chosen = index_pixel_chosen_list(i_pixel);
        img1 = eng_map_cov_temp(:, index_pixel_chosen);
        img1 = reshape(img1, map_size);
        figure(6+i_pixel);
        imagesc(img1, clims); axis equal; colorbar; colormap gray;
        if flag_title
            title(['Covariance w.r.t. pixel (', num2str(index_pixel_x_chosen), ', ', num2str(index_pixel_y_chosen), ')']);
        end
        map_plot_resize;
    end
end
%% Save
flag_print = false;
if flag_print
    print_folder = './temp/';
    if flag_print_cov
        end_plot = 8;
    else
        end_plot = 5;
    end
    for index_plot = 3:end_plot
        thefigure = figure(index_plot);
        print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(300)]);
        % saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
end
