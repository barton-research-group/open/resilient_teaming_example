close all;

rng(13000);

gt_map_thres = 50;
map_size = [100, 100];
map_res = 10;
sample_noise = 0;

eng_map_sigma_f = 6;
eng_map_sigma_white = 0;


eng_map_lambda = 100;
eng_map_prior_mean = 30;
sample_num = 80;

gpr = GPRegressor();
% gpr.fit(sample_loc, sample_eng, eng_map_lambda, eng_map_prior_mean, eng_map_sigma_f, eng_map_sigma_white);
gpr.set(eng_map_lambda, eng_map_prior_mean, eng_map_sigma_f, eng_map_sigma_white);
[y_rand, y_interp] = gpr.gen_map(map_size, map_res);

y_interp = round(y_interp);
y_interp = max(y_interp, 10);
y_interp = min(y_interp, 50);

y_interp(16:25, 76:85) = 60; % Add one obstacle
y_interp(76:85, 16:25) = 60; % Add one obstacle


figure('units','normalized');
imagesc(y_rand); axis equal; colorbar; colormap jet;

figure('units','normalized');
imagesc(y_interp); axis equal; colorbar; colormap jet;

%%
csvwrite('data/map.txt', y_interp)
