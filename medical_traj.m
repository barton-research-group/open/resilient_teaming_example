close all;

vehicle_plot_list = [1, 2, 3];

plot_file = '../result/medical/con_v21_m16_a1/result.yaml';
mat_file = '../test/medical/con_v21_m16_a1/matlog.mat';

% plot_file = '../resilient_team_planner/build/medical_con_result.yaml';
% mat_file = '../resilient_team_planner/config/medical_con/matlog.mat';
%% Intialize parameters
flag_plot_map = true;
addpath('lib/YAMLMatlab_0.4.3/');
load(mat_file);
result = ReadYaml(plot_file);
x = cell2mat(result.xVar); x = x(:);

background_color = [0, 0, 0];
the_color = [0,0.447000000000000,0.741000000000000;0.850000000000000,0.325000000000000,0.0980000000000000;0.929000000000000,0.694000000000000,0.125000000000000;0.494000000000000,0.184000000000000,0.556000000000000;0.466000000000000,0.674000000000000,0.188000000000000;0.301000000000000,0.745000000000000,0.933000000000000;0.635000000000000,0.0780000000000000,0.184000000000000];

% font_size = 20;
% font_size_text = 25;
font_size = 20;
font_size_text = 20;
font_name = 'Times New Roman';
line_width = 1.5;
marker_size = 10;
% set(0,'defaultAxesFontSize',10)
flag_no_axis = true;

flag_cont_plot = false;
if exist('flag_cont')
    if flag_cont
        flag_cont_plot = true;
    end
end

%% Show the path
figure('units','normalized');
eng_range = [10, 100];
imagesc(eng_map, eng_range); axis equal; colorbar;
median_range = [min(eng_map(:)), eng_map_prior_mean-eng_map_margin, eng_map_prior_mean+eng_map_margin, max(eng_map(:))];
color_map = my_colormap(1, median_range);
colormap(color_map);
hold on;
plot(pose_data(:,1), pose_data(:,2), '.', 'Color', [0.5, 0.5, 0.5]);
set(gca, 'ydir', 'normal'); % reverse, normal

c_offset = 1;

vehicle_list = fieldnames(result.vehicle);
index_mid = (length(vehicle_plot_list)/2+0.5);

if flag_cont_plot
    type = 1;
    for index_vehicle_plot = 1:length(vehicle_plot_list)
        index_vehicle = vehicle_plot_list(index_vehicle_plot);
        node_list = cell2mat(result.vehicle.(vehicle_list{index_vehicle}).node);
        for index_node = 1:length(node_list)-1
            veh_type = result.vehicle.(vehicle_list{index_vehicle}).type;
            index_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, veh_type, node_list(index_node), node_list(index_node+1));
            x_offset = (index_vehicle_plot - index_mid) * c_offset;
            y_offset = x_offset;
            plot(fpath{index_x}(:,1) + x_offset, fpath{index_x}(:,2) + y_offset, 'LineWidth', 3, 'Color', the_color(index_vehicle_plot, :));
            hold on;
        end
    end
else
    type = 1;
    for index_vehicle = vehicle_plot_list
        ind_in = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, -1, -1);
        for index_x = ind_in.'
            if x(index_x) > 0.5
                plot(fpath{index_x}(:,1), fpath{index_x}(:,2), 'k', 'LineWidth', 7);
                hold on;
                plot(fpath{index_x}(:,1), fpath{index_x}(:,2), 'w--', 'LineWidth', 3);
            end
        end
    end
end

plot(mission_loc_int_list(:, 1), mission_loc_int_list(:, 2), 'bx', 'LineWidth', 3, 'MarkerSize', marker_size);
plot(start_loc_int_list(:, 1), start_loc_int_list(:, 2), 'gx', 'LineWidth', 3, 'MarkerSize', marker_size);
plot(terminal_loc_int_list(:, 1), terminal_loc_int_list(:, 2), 'gx', 'LineWidth', 3, 'MarkerSize', marker_size);

xy_offset = 1;

for index_mission = 1:n_mission
    t = text(mission_loc_int_list(index_mission, 1) + xy_offset, mission_loc_int_list(index_mission, 2) + xy_offset, ['${{', num2str(index_mission),'}}$'], 'Color', 'k', 'FontSize', font_size_text, 'FontName', font_name, 'Interpreter', 'latex');
end
t = text(base_pose(1), base_pose(2), 'Base', 'Color', 'k', 'FontSize', font_size, 'FontName', font_name, 'Interpreter', 'latex');

hold off;

map_plot_resize;

if flag_no_axis
    set(gca,'ytick',[]);
    set(gca,'xtick',[]);
    set(gca,'visible','off');
end


%% Save
flag_print = true;
if flag_print
    print_folder = ['./temp/'];
    for index_plot = [1]
        thefigure = figure(index_plot);
        print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(300)]);
        % print(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf'],'-dpdf');
        % saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.fig']);
    end
end