clear;
clc;
close all;

% Set parameters
thres_upper = 1.3;  % Remove edges that are longer than this distance
thres_lower = 0.25; % Remove edges that are shorter than this distance
pose_offset = [50, 45.9057+1];

pose_data = load('data/2D/M3500_pose.txt');
pose_data = pose_data(:,2:3) + pose_offset;

%% Merge too close poses
dist_mat = pdist2(pose_data, pose_data, 'euclidean');
dist_mat_bool = dist_mat < thres_lower;

use_ind = find(dist_mat_bool);
[edge_i, edge_j] = ind2sub(size(dist_mat), use_ind);
edge_data = [edge_i, edge_j];

[parent_id] = my_conncomp(edge_data(:,2), edge_data(:,1));
[pose_use, pose_ie, pose_ic] = unique(parent_id);

pose_data = pose_data(pose_use, :);

%% Link poses with edges
dist_mat = pdist2(pose_data, pose_data, 'euclidean');
dist_mat_bool = (dist_mat < thres_upper) & (dist_mat > thres_lower);

use_ind = find(dist_mat_bool);
[edge_i, edge_j] = ind2sub(size(dist_mat), use_ind);
edge_data = [edge_j, edge_i];

[parent_id] = my_conncomp(edge_data(:,1), edge_data(:,2));
flag_connect = sum(parent_id ~= 1) == 0;
if flag_connect
    disp('The whole graph is connected.')
else
    disp('The graph is not connected.')
end

%% Contruct the edge information
edge_xy1 = pose_data(edge_data(:,1), :);
edge_xy2 = pose_data(edge_data(:,2), :);

edge_dxy = edge_xy2 - edge_xy1;
edge_dist = sqrt(sum(edge_dxy.^2, 2));
edge_x = [edge_xy1(:,1), edge_xy2(:,1)];
edge_y = [edge_xy1(:,2), edge_xy2(:,2)];


%% Show map
figure(1);
plot(pose_data(:,1), pose_data(:,2), 'k.');
hold on;
plot(edge_x.', edge_y.', 'Color', [1, 1, 1] * 0.7);
hold off;
axis equal;
title('Map');

%% Show edge length
figure(2);
plot(edge_dist);
xlabel('Edge id');
ylabel('Edge length');

%% Save
save('data/M3500.mat', 'pose_data', 'edge_data', 'edge_dist', 'edge_xy1', 'edge_xy2');

%% Print
flag_print = true;
if flag_print
    print_folder = ['./temp/'];
    for index_plot = 1
        thefigure = figure(index_plot);
        print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(400)]);
        % print(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf'],'-dpdf');
        % saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
end

