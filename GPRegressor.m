classdef GPRegressor < matlab.mixin.Copyable
    %UNTITLED Summary of this class goes here
    % This is a handle class
    %   Detailed explanation goes here
    
    properties
        K = []
        K_star = []
        I = []
        X = []
        y = []
        alpha = []
        L = []
        l = 1
        sigma_f = 1
        sigma_noise = 1e-10
        sigma_white = 1
        my_eps = 1e-6;
        prior_mean = 0;
    end
    %% Methods
    methods
        function obj = GPRegressor()
            %Construct an instance
            obj.K = [];
            obj.K_star = [];
            obj.I = [];
            obj.X = [];
            obj.y = [];
            obj.alpha = [];
            obj.l = 1;
            obj.sigma_f = 1;
            obj.sigma_noise = 1e-10;
            obj.sigma_white = 1;
        end
        
        function set(obj, l, prior_mean, sigma_f, sigma_white)
            obj.l = l;
            obj.prior_mean = prior_mean;
            obj.sigma_f = sigma_f;
            obj.sigma_white = sigma_white;
        end
        
        function [y_rand, y_interp] = gen_map(obj, map_size, map_res)
            x2_list_int = 1:map_size(1);
            x1_list_int = 1:map_size(2);
            x2_list = x2_list_int * map_res;
            x1_list = x1_list_int * map_res;
            [x1_grid, x2_grid] = meshgrid(x1_list, x2_list);
            X12 = [x1_grid(:), x2_grid(:)];
            
            N = size(X12, 1);
            kxx = obj.get_rbf(X12);
            cholK = chol(kxx + eye(N)*1e-8, 'lower');
            y_rand = obj.prior_mean * ones(N,1) + cholK *  randn(N,1);   
            y_rand = reshape(y_rand, map_size);
            
            y_interp = interp2(x1_grid, x2_grid, y_rand, x1_grid, x2_grid, 'spline');            
        end
        
        function fit(obj, X, y, l, prior_mean, sigma_f, sigma_white)
            if nargin >= 4
                obj.l = l;
            end
            if nargin >= 5
                obj.prior_mean = prior_mean;
            end
            if nargin >= 6
                obj.sigma_f = sigma_f;
            end
            if nargin >= 7
                obj.sigma_white = sigma_white;
            end
            obj.K = obj.get_kernel(X);
            obj.I = eye(size(X, 1));
            obj.X = X;
            obj.y = y - obj.prior_mean;
            n_size = size(obj.K); index_diag = sub2ind(n_size, 1:n_size(1), 1:n_size(1));
            obj.K(index_diag) = obj.K(index_diag) + obj.sigma_noise;
            obj.L = chol(obj.K, 'lower'); % K = L*L'
            obj.alpha = obj.K \ obj.y; % alpha = K^-1 * y
        end
        
        function [y_star, y_cov] = predict(obj, X_star)
            K_star_trans = obj.get_kernel(X_star, obj.X);
            y_star = K_star_trans * obj.alpha + obj.prior_mean;
            v = obj.L \ (K_star_trans.');
            y_cov = obj.get_kernel(X_star, X_star) - v.' * v;
            % The above two lines have the same functionality as
            % y_cov = obj.get_kernel(X_star, X_star) - K_star_trans / obj.K * K_star_trans.';
        end
        
        function K_star = get_rbf(obj, X, X_star)
            if nargin < 3
                X_star = [];
            end
            if isempty(X_star)
                dist_mat = pdist(X, 'squaredeuclidean'); % distance between rows of X
                K_star = obj.sigma_f^2 * exp(-0.5 / obj.l^2 * dist_mat);
                K_star = squareform(K_star);
                n_size = size(K_star); index_diag = sub2ind(n_size, 1:n_size(1), 1:n_size(1));
                K_star(index_diag) = obj.sigma_f^2;
            else
                dist_mat = pdist2(X / obj.l, X_star / obj.l, 'squaredeuclidean'); % distance between rows of X and X_star
                K_star = obj.sigma_f^2 * exp(-0.5 * dist_mat);
            end
        end
        
        
        function K_star = get_white(obj, X, X_star)
            if nargin < 3
                X_star = [];
            end
            if isempty(X_star)
                dist_mat = pdist(X, 'squaredeuclidean'); % distance between rows of X
                K_star = obj.sigma_white^2 * double(dist_mat < obj.my_eps);
                K_star = squareform(K_star);
                n_size = size(K_star); index_diag = sub2ind(n_size, 1:n_size(1), 1:n_size(1));
                K_star(index_diag) = obj.sigma_white^2;
            else
                % dist_mat = pdist2(X / obj.l, X_star / obj.l, 'squaredeuclidean'); % distance between rows of X and X_star
                % K_star = obj.sigma_white^2 * double(dist_mat < obj.my_eps);
                K_star = zeros(size(X,1), size(X_star,1));
            end
        end
        
        function K_star = get_kernel(obj, X, X_star)
            if nargin < 3
                X_star = [];
            end
            K_star = obj.get_rbf(X, X_star) + obj.get_white(X, X_star);
        end
        
    end
end

