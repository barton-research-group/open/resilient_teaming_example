% set(gcf, 'Position',  image_pos);
xlim([0,map_size(2)]);
ylim([0,map_size(1)]);
xlabel('$x$', 'Interpreter', 'Latex', 'FontSize', font_size, 'FontName', font_name);
ylabel('$y$', 'Interpreter', 'Latex', 'FontSize', font_size, 'FontName', font_name);
set(gca, 'FontSize', font_size);
set(gca, 'FontName', font_name)
xtick_list = get(gca,'XTick');
set(gca,'XTickLabel', xtick_list * map_res);
ytick_list = get(gca,'YTick');
set(gca,'YTickLabel', ytick_list * map_res);