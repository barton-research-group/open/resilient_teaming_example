%% ARC Project Example with Time
close all;
clear;
clc;

% seed = randi(100000)
% rng(seed);
rng(78017);

%% Initialize data
n_vehicle_rep = 3;
n_mission_rep = 2;
n_req = 1;
flag_cont = true;

n_cap = 9;
n_av = 7;
n_am = 8;

flag_edge_time_cost = false;
base_node = 228; % The 228th pose

if flag_cont
    n_vehicle_rep_cont = n_vehicle_rep;
    n_vehicle_rep = 1;
else
    n_vehicle_rep_cont = 1;
end

n_mission = n_am * n_mission_rep;
n_vehicle = n_av * n_vehicle_rep;

% Vehicle
vehicle_eng_cap = [1, 1, 1, 1, 1, 1, 1].' * 1000 * 1000;
vehicle_eng_cap = repmat(vehicle_eng_cap, [n_vehicle_rep,1]);

vehicle_cost = [2, 2, 2, 4, 8, 3, 4].';
vehicle_cost = repmat(vehicle_cost, [n_vehicle_rep,1]);

vehicle_vel = 2;

vehicle_cap = [1	0	1	1	0	0	0	1	0
    0	0	1	0	0	0	0	0	2
    0	1	1	0	0	0	0	0	0
    0	0	0	0	1	0	0	1	0
    0	0	0	1	0	0	0	0	5
    0	0	0	0	0	1	0	0	0
    0	0	0	0	0	1	1	0	0];
vehicle_cap = repmat(vehicle_cap, [n_vehicle_rep,1]);

cap_cumulative = zeros(1, n_cap);
cap_cumulative(1) = 1; % Non-cumulative

% Task requirements
mission_cap_type = cell(n_am, n_cap);
mission_cap_num = cell(n_am, n_cap);
mission_cap_type{1,1} = 3; mission_cap_num{1,1} = 1 * n_req;
mission_cap_type{2,1} = 1; mission_cap_num{2,1} = 1;
mission_cap_type{2,2} = 3; mission_cap_num{2,2} = 1 * n_req;
mission_cap_type{3,1} = 2; mission_cap_num{3,1} = 1 * n_req;
mission_cap_type{3,2} = 3; mission_cap_num{3,2} = 1 * n_req;
mission_cap_type{4,1} = 5; mission_cap_num{4,1} = 1 * n_req;
mission_cap_type{4,2} = 8; mission_cap_num{4,2} = 1 * n_req;
mission_cap_type{5,1} = 4; mission_cap_num{5,1} = 1 * n_req;
mission_cap_type{5,2} = 5; mission_cap_num{5,2} = 1 * n_req;
mission_cap_type{5,3} = 8; mission_cap_num{5,3} = 2 * n_req;
mission_cap_type{6,1} = 3; mission_cap_num{6,1} = 1 * n_req;
mission_cap_type{6,2} = 4; mission_cap_num{6,2} = 1 * n_req;
mission_cap_type{6,3} = 5; mission_cap_num{6,3} = 1 * n_req;
mission_cap_type{6,4} = 8; mission_cap_num{6,4} = 1 * n_req;
mission_cap_type{6,5} = 9; mission_cap_num{6,5} = 10 * n_req;

mission_cap_type{7,1} = 6; mission_cap_num{7,1} = 1 * n_req;
mission_cap_type{8,1} = 6; mission_cap_num{8,1} = 1 * n_req;
mission_cap_type{8,2} = 7; mission_cap_num{8,2} = 1 * n_req;

mission_cap_type = repmat(mission_cap_type, [n_mission_rep, 1]);
mission_cap_num = repmat(mission_cap_num, [n_mission_rep, 1]);

% Map

flag_dist_cost_list = false(n_vehicle, 1);

c_dist_cost = 60;
sigma_dist_cost = 1;

mission_dist = squareform( pdist((1:n_mission).') );
mission_link_map = mission_dist > 0;

[mission_from, mission_to] = find(mission_link_map);
mission_sub2ind = sparse(mission_from, mission_to, (1:size(mission_from,1)).', n_mission, n_mission);
mission_ind2sub = [mission_from, mission_to];
n_link = size(mission_ind2sub, 1);

edge_time_cost = ones((n_link + 2*n_mission) * n_vehicle, 1);
mission_time_cost = ones(n_vehicle, n_mission) * 20;

%% Load map
load('data/M3500.mat'); % pose_data, edge_data
load('data/M3500_gpr.mat');
n_pose = size(pose_data, 1);
n_mapedge = size(edge_data, 1);
mapedge_cost = edge_dist .* edge_unit_eng;
G = digraph(edge_data(:,1), edge_data(:,2), mapedge_cost);
mapedge_id = sparse(edge_data(:,1), edge_data(:,2), 1:n_mapedge, n_pose, n_pose);

%% Calculate Numbers
n_xvar = (n_link + 2*n_mission) * n_vehicle;
n_repeat = (n_link + 2*n_mission) * n_av;

%% Generate map
edge_cost = zeros(n_xvar, 1);
edge_var = zeros(n_xvar, 1);
fpath = cell(n_xvar, 1);
edge_v = zeros(n_xvar, 1);
edge_i = zeros(n_xvar, 1);
edge_j = zeros(n_xvar, 1);
edge_type = ones(n_xvar, 1);
medical_map;

clear edge_unit_eng_cov;
%% Save graph
mat_test_folder = '../temp/test/';
cpp_test_folder = '../temp/../test/';
% sub_folder = 'case1/';
if flag_cont
    sub_folder = ['temp/2/', 'con_v', num2str(n_av * n_vehicle_rep_cont), '_m', num2str(n_mission), '_a', num2str(n_req), '/'];
else
    sub_folder = ['temp/2/', 'int_v', num2str(n_vehicle), '_m', num2str(n_mission), '_a', num2str(n_req), '/'];
end
mkdir([mat_test_folder, sub_folder]);
% Save graph file
graph_file = [mat_test_folder, sub_folder, 'graph.yaml'];
save_graph(graph_file, edge_v, edge_i, edge_j, edge_type, edge_cost, edge_var, edge_time_cost);
% Save agent file
vehicle_file = [mat_test_folder, sub_folder, 'vehicle_param.yaml'];
save_vehicle(vehicle_file, vehicle_eng_cap, vehicle_cost, vehicle_cap);
% Save task file
task_file = [mat_test_folder, sub_folder, 'task_param.yaml'];
save_task(task_file, mission_cap_type, mission_cap_num);
% Save planner parameters
planner_file = [mat_test_folder, sub_folder, 'planner_param.yaml'];
save_plannerparam(planner_file, [cpp_test_folder, sub_folder], n_vehicle, n_mission, n_cap, n_av, n_vehicle_rep_cont, cap_cumulative, flag_cont);

%% Save mat files
mat_file = [mat_test_folder, sub_folder, 'matlog.mat'];
save(mat_file);

%% Print
cpp_input_file = [cpp_test_folder, sub_folder, 'planner_param.yaml'];
mat_result_folder = '../temp/result/';
cpp_result_folder = '../temp/../result/';
mkdir([mat_result_folder, sub_folder]);
output_file = [cpp_result_folder, sub_folder, 'result.yaml'];
fprintf("./main %s %s\n", cpp_input_file, output_file);
plot_file = [mat_result_folder, sub_folder, 'result.yaml'];

