% rng(1000);

map_y_size = 50;
map_x_size = 100;

% start_loc_list = [[-10, 10, -10, 10].' + map_x_size / 2, [-10, -10, 10, 10].' + map_y_size / 2];
% start_loc_list = [[-20, -10, 0, 10, -20, -10, 0, 10].' + map_x_size / 2, [-10, -10, -10, -10, 10, 10, 10, 10].' + map_y_size / 2];
% start_loc_list = [[-10, 10, 20, -10, 10, 20].' + map_x_size / 2, [-10, -10, -10, 10, 10, 10].' + map_y_size / 2];
% start_loc_list = [[-10, 10, 20, 25, 30, -10, 10, 20, 25, 30].' + map_x_size / 2, [-10, -10, -10, -10, -10, 10, 10, 10, 10, 10].' + map_y_size / 2];


mission_locx_list = rand(n_mission, 1) * map_x_size;
mission_locy_list = rand(n_mission, 1) * map_y_size;
mission_loc_list = [mission_locx_list, mission_locy_list];

start_loc_list = zeros(n_vehicle, 2) + [map_x_size, map_y_size / 2];
terminal_loc_list = zeros(n_vehicle, 2) + [0, map_y_size / 2];

start_loc_int_list = start_loc_list;
terminal_loc_int_list = terminal_loc_list;
mission_loc_int_list = mission_loc_list;

for index_vehicle = 1:n_vehicle
    for index_mission = 1:n_mission
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, 0, index_mission);
        start_loc = start_loc_list(index_vehicle, :).';
        goal_loc = mission_loc_list(index_mission, :).';
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = index_vehicle + n_mission;
        edge_j(ind_x) = index_mission;
        fpath{ind_x} = [start_loc.'; goal_loc.'];
        edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost;
        edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2;
    end
    for index_mission = 1:n_mission
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, index_mission, 0);
        start_loc = mission_loc_list(index_mission, :).';
        goal_loc = terminal_loc_list(index_vehicle, :).';
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = index_mission;
        edge_j(ind_x) = index_vehicle + n_vehicle + n_mission;
        fpath{ind_x} = [start_loc.'; goal_loc.'];
        edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost;
        edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2;
    end
    for index_link = 1:size(mission_ind2sub, 1)
        type = 1;
        ind_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, mission_ind2sub(index_link, 1), mission_ind2sub(index_link, 2));
        start_loc = mission_loc_list(mission_ind2sub(index_link, 1), :).';
        goal_loc = mission_loc_list(mission_ind2sub(index_link, 2), :).';
        edge_v(ind_x) = index_vehicle;
        edge_i(ind_x) = mission_ind2sub(index_link, 1);
        edge_j(ind_x) = mission_ind2sub(index_link, 2);
        fpath{ind_x} = [start_loc.'; goal_loc.'];
        edge_cost(ind_x) = norm(start_loc - goal_loc) * c_dist_cost;
        edge_var(ind_x) = (norm(start_loc - goal_loc) * sigma_dist_cost)^2;
    end
end
