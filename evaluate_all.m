addpath('lib/YAMLMatlab_0.4.3/');
clear;

testId = 1;
flagSolverList = {'TEAMPLANNER_DET', 'TEAMPLANNER_SPRITER', 'TEAMPLANNER_CCPITER', 'TEAMPLANNER_SPR', 'TEAMPLANNER_CCP'};

baseDataFolder = '/Users/bofu/Desktop/gurobi_start/code/test/';


nvList = {'6', '8', '10', '20', '50'};
nvLeft  = 'nv/v';
nvRight = '_m6_a2_av2_am3_mu30_sig6_';

nmList = {'6', '9', '12', '15', '18', '24', '30'};
nmLeft  = 'nm/v6_m';
nmRight = '_a2_av2_am3_mu30_sig6_';

namList = {'1', '2', '3', '4', '5', '6'};
namLeft  = 'nam/v6_m6_a3_av3_am';
namRight = '_mu30_sig6_';

sigList = {'3', '6', '9', '12', '15'};
sigLeft  = 'sig/v6_m6_a2_av2_am3_mu30_sig';
sigRight = '_';


useLeft  = nvLeft;
useRight = nvRight;
useList = nvList;

switch testId
    case 0
        useLeft  = nvLeft;
        useRight = nvRight;
        useList = nvList;
    case 1
        useLeft  = nmLeft;
        useRight = nmRight;
        useList = nmList;
    case 2
        useLeft  = namLeft;
        useRight = namRight;
        useList = namList;
    case 3
        useLeft  = sigLeft;
        useRight = sigRight;
        useList = sigList;
    case 4
        useLeft  = nm2Left;
        useRight = nm2Right;
        useList = nm2List;
    otherwise
end

n_case = length(useList);
n_solver = length(flagSolverList);
n_rep = 5;
n_vehicle = 50;

result.time = zeros(n_case, n_solver);
result.f = zeros(n_case, n_solver);
result.g = zeros(n_case, n_solver);
result.fg = zeros(n_case, n_solver);
result.gap = zeros(n_case, n_solver);
result.succ = zeros(n_case, n_solver);

result.time_var = zeros(n_case, n_solver);
result.f_var = zeros(n_case, n_solver);
result.g_var = zeros(n_case, n_solver);
result.fg_var = zeros(n_case, n_solver);
result.gap_var = zeros(n_case, n_solver);

result.f1 = zeros(n_case, n_solver);
result.g1 = zeros(n_case, n_solver);
result.fg1 = zeros(n_case, n_solver);

result.f1_var = zeros(n_case, n_solver);
result.g1_var = zeros(n_case, n_solver);
result.fg1_var = zeros(n_case, n_solver);

for i_case = 1:n_case
    for i_solver = 1:n_solver
        time_temp = zeros(n_rep, 1);
        f_temp = zeros(n_rep, 1);
        g_temp = zeros(n_rep, 1);
        fg_temp = zeros(n_rep, 1);
        gap_temp = zeros(n_rep, 1);
        i_succ = 0;
        f1_temp = zeros(n_rep, 1);
        g1_temp = zeros(n_rep, 1);
        fg1_temp = zeros(n_rep, 1);
        for i_rep = 1:n_rep
            subDataFolder = [useLeft, useList{i_case}, useRight, num2str(i_rep), '/'];
            resultFolder = ['result_use/', subDataFolder];
            resultFileName = [baseDataFolder, resultFolder, flagSolverList{i_solver}, '.yaml'];
            if exist(resultFileName, 'file') ~= 2
                fprintf('Not Exist: resultFileName: %s\n', resultFileName);
                continue;
            end
            resultFile = ReadYaml(resultFileName);
            if resultFile.result.flagSuccess < 0.5
                fprintf('Fail: %s\n', resultFileName);
                continue;
            end
            vf_temp = 0;
            vg_temp = 0;
            for i_veh = 1:n_vehicle
                if ~isfield(resultFile.vehicle, ['vv', num2str(i_veh)])
                    break;
                end
                vf_temp = vf_temp + resultFile.vehicle.vv1.vehEngMu;
                vg_temp = vg_temp + resultFile.vehicle.vv1.vehRecourseCost;
            end
            i_succ = i_succ + 1;
            time_temp(i_succ) = resultFile.result.solverTime;
            f_temp(i_succ) = resultFile.result.fCost;
            g_temp(i_succ) = resultFile.result.recourseCost;
            fg_temp(i_succ) = resultFile.result.totalCost;
            gap_temp(i_succ) = resultFile.result.MIPGap;

            f1_temp(i_succ) = vf_temp;
            g1_temp(i_succ) = vg_temp;
            fg1_temp(i_succ) = vf_temp + vg_temp;

        end
        result.time(i_case, i_solver) = mean(time_temp(1:i_succ));
        result.f(i_case, i_solver) = mean(f_temp(1:i_succ));
        result.g(i_case, i_solver) = mean(g_temp(1:i_succ));
        result.fg(i_case, i_solver) = mean(fg_temp(1:i_succ));
        result.gap(i_case, i_solver) = mean(gap_temp(1:i_succ));

        result.time_var(i_case, i_solver) = std(time_temp(1:i_succ));
        result.f_var(i_case, i_solver) = std(f_temp(1:i_succ));
        result.g_var(i_case, i_solver) = std(g_temp(1:i_succ));
        result.fg_var(i_case, i_solver) = std(fg_temp(1:i_succ));
        result.gap_var(i_case, i_solver) = std(gap_temp(1:i_succ));

        result.f1(i_case, i_solver) = mean(f1_temp(1:i_succ));
        result.g1(i_case, i_solver) = mean(g1_temp(1:i_succ));
        result.fg1(i_case, i_solver) = mean(fg1_temp(1:i_succ));
        result.f1_var(i_case, i_solver) = std(f1_temp(1:i_succ));
        result.g1_var(i_case, i_solver) = std(g1_temp(1:i_succ));
        result.fg1_var(i_case, i_solver) = std(fg1_temp(1:i_succ));

        result.succ(i_case, i_solver) = i_succ;        
    end
end
