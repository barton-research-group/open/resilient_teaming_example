function [mission_cap_type, mission_cap_num] = mission_cap_to_typenum(mission_cap)

[n_mission, n_cap] = size(mission_cap);
mission_cap_type = cell(n_mission, n_cap);
mission_cap_num = cell(n_mission, n_cap);
for i = 1:n_mission
    k = 1;
    for j = 1:n_cap
        if mission_cap(i,j)
            mission_cap_type{i,k} = j;
            mission_cap_num{i,k} = 1;
            k = k + 1;
        end
    end
end

end