function [flag_valid, n_ieq_more, Aieq_i_more, Aieq_j_more, Aieq_v_more, bieq_more, path_mu, path_sigma] = check_eng_ccp(x, index_x, beta, vehicle_eng_cap, edge_mu, edge_var, n_ieq, n_vehicle)

x_b = zeros(size(x));
x_b(index_x) = double(x(index_x) > 0.5);

path_sigma = sqrt(x_b(index_x).' * edge_var(index_x) );
path_mu = x_b(index_x).' *  edge_mu(index_x);
flag_valid = (norminv(beta) * path_sigma + path_mu) <= vehicle_eng_cap;

% if flag_valid
%     n_ieq_more = n_ieq;
%     Aieq_i_more = [];
%     Aieq_j_more = [];
%     Aieq_v_more = [];
%     bieq_more = [];
% else
%     N1 = sum(x_b(index_x));
%     n_ieq_more = n_ieq + 1;
%     Aieq_i_more = [ones(N1, 1) * n_ieq_more];
%     Aieq_j_more = [find(x_b)];
%     Aieq_v_more = [ones(N1, 1)];
%     bieq_more = [N1 - 1];
% end
% 

n_ieq_more = n_ieq;
Aieq_i_more = [];
Aieq_j_more = [];
Aieq_v_more = [];
bieq_more = [];
if ~flag_valid
    N1 = sum(x_b(index_x));
    n_iter = length(index_x);
    n_offset = index_x(1) - 1;
    index_first = find(x_b) - n_offset;
    for index_vehicle = 1:n_vehicle
        n_ieq_more = n_ieq_more + 1;
        Aieq_i_more = [Aieq_i_more; ones(N1, 1) * n_ieq_more];
        Aieq_j_more = [Aieq_j_more; n_iter * (index_vehicle - 1) + index_first];
        Aieq_v_more = [Aieq_v_more; ones(N1, 1)];
        bieq_more = [bieq_more; N1 - 1];
    end
end

end