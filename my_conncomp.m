function [parent_id] = my_conncomp(edge1, edge2)


n_node = max([edge1; edge2]);
parent_id = (1:n_node).';
parent_contains = cell(n_node, 1);
for i_pose = 1:n_node
    parent_contains{i_pose} = i_pose;
end
for i_edge = 1:length(edge1)
    i1 = edge1(i_edge);
    i2 = edge2(i_edge);
    p1 = parent_id(i1);
    p2 = parent_id(i2);
    if (p1 == p2)
        continue;
    end
    % Merge
    if length(parent_contains{p1}) >= length(parent_contains{p2})
        % Merge 2 into 1
        parent_id(parent_contains{p2}) = p1;
        parent_contains{p1} = [parent_contains{p1}; parent_contains{p2}];
        parent_contains{p2} = [];
    else
        % Merge 2 into 1
        parent_id(parent_contains{p1}) = p2;
        parent_contains{p2} = [parent_contains{p2}; parent_contains{p1}];
        parent_contains{p1} = [];
    end    
end


end