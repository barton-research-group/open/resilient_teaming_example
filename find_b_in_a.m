function [index, n_iter] = find_b_in_a(a, b)

n_a = size(a, 1);
n_b = size(b, 1);

n_iter = 0;

index = zeros(n_b, 1);

i_a_curr = 1;
for i_b = 1:n_b
    i_use = [i_a_curr:n_a, 1:(i_a_curr-1)];
    for i_a = i_use
        n_iter = n_iter + 1;
        diff_ab = norm(a(i_a, :) - b(i_b, :));
        if (diff_ab < 1e-6)
            index(i_b) = i_a;
            i_a_curr = i_a + 1;
            break;
        end
    end
end


end