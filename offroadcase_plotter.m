% close all;
addpath('lib/YAMLMatlab_0.4.3/');
result = ReadYaml(plot_file);
x = cell2mat(result.xVar); x = x(:);

eng_range = [min(gt_map(:)), max(gt_map(:))];

font_size = 30;
font_size_text = 35;
font_name = 'Times New Roman';
line_width = 1.5;
marker_size = 20;
% set(0,'defaultAxesFontSize',10)

figure('units','normalized');
imagesc(eng_map, eng_range); axis equal; colorbar; colormap jet; hold on;

type = 1;
for index_vehicle = [5]
    ind_in = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, -1, -1);
    for index_x = ind_in.'
        if x(index_x) > 0.5
            plot(fpath{index_x}(:,1), fpath{index_x}(:,2), 'k', 'LineWidth', 7);
            plot(fpath{index_x}(:,1), fpath{index_x}(:,2), 'w--', 'LineWidth', 3);
        end
    end
end
% for index_x = 1:n_xvar
%     if x(index_x) > 0.5
%         plot(fpath{index_x}(:,1), fpath{index_x}(:,2), 'k', 'LineWidth', 2);
%     end
% end
plot(start_loc_int_list(:, 1), start_loc_int_list(:, 2), 'gx', 'LineWidth', 4, 'MarkerSize', marker_size);
plot(terminal_loc_int_list(:, 1), terminal_loc_int_list(:, 2), 'gx', 'LineWidth', 4, 'MarkerSize', marker_size);
plot(mission_loc_int_list(:, 1), mission_loc_int_list(:, 2), 'mx', 'LineWidth', 4, 'MarkerSize', marker_size);

xy_offset = 0.5;
% for index_vehicle = 1:n_vehicle
%     text(start_loc_int_list(index_vehicle, 1) + xy_offset, start_loc_int_list(index_vehicle, 2) + xy_offset, ['S', num2str(index_vehicle)], 'Color', 'w');
% end

for index_mission = 1:n_mission
%     t = text(mission_loc_int_list(index_mission, 1) + xy_offset, mission_loc_int_list(index_mission, 2) + xy_offset, ['m_{', num2str(index_mission),'}'], 'Color', 'k', 'FontSize', font_size_text, 'FontName', font_name, 'FontAngle', 'italic');%, 'Interpreter', 'latex'
    t = text(mission_loc_int_list(index_mission, 1) + xy_offset, mission_loc_int_list(index_mission, 2) + xy_offset, ['${m_{', num2str(index_mission),'}}$'], 'Color', 'k', 'FontSize', font_size_text, 'FontName', font_name, 'Interpreter', 'latex');
end

% index_vehicle = 1;
% text(terminal_loc_int_list(index_vehicle, 1) + xy_offset, terminal_loc_int_list(index_vehicle, 2) + xy_offset, 'T', 'Color', 'w');

hold off;

map_plot_resize;
% xlim([0, map_size(2)]);
% ylim([0, map_size(1)]);
% axis equal;

%% Save
flag_print = false;
if flag_print
    %pause;
    print_folder = ['./temp/'];
    for index_plot = 1
        thefigure = figure(index_plot);
%         print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(300)]);
%         print(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf'],'-dpdf');
        saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
%     close all;
end