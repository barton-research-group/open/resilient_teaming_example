close all;
clear;
clc;
addpath('build/');

flag_title = false;
flag_save_map_data = false;
flag_save_cov = false;

seed = 2000;
rng(seed);

gt_map = load('data/map.txt');
save_name = 'data/map_data.mat';

n_resize = 1;
if n_resize > 1.5
    gt_map = imresize(gt_map , n_resize);
end
gt_map_thres = 59;
map_size = size(gt_map);
map_res = 10;
sample_noise = 0;

eng_map_sigma_f = 6;
eng_map_sigma_white = 1;

%% Gaussian Process Registration
eng_map_lambda = 100;
eng_map_prior_mean = 30;
sample_num = 100;

index_sample = randperm(numel(gt_map), sample_num).';
index_sample_obs = gt_map(index_sample) >= gt_map_thres;
index_sample(index_sample_obs) = [];

index_valid = gt_map < gt_map_thres;
index_obs = ~index_valid;
index_valid_cumsum = cumsum(index_valid(:));

[index_y, index_x] = ind2sub(map_size, index_sample);

sample_loc = [index_x, index_y] * map_res;
sample_eng = gt_map(index_sample) + sample_noise * randn(size(index_sample));

gpr = GPRegressor();
tic;
gpr.fit(sample_loc, sample_eng, eng_map_lambda, eng_map_prior_mean, eng_map_sigma_f, eng_map_sigma_white);
fit_time = toc;

[xx, yy] = meshgrid(1:map_size(2), 1:map_size(1));

eng_map = zeros(map_size);
eng_map(index_obs) = gt_map(index_obs);

eng_map_var = zeros(map_size);
tic;
[eng_map_temp, eng_map_cov_temp] = gpr.predict([xx(index_valid)*map_res, yy(index_valid)*map_res]);
predict_time = toc;
eng_map_var_temp = diag(eng_map_cov_temp);
eng_map(index_valid) = eng_map_temp;
eng_map_var(index_valid) = eng_map_var_temp;
if flag_save_cov
    eng_map_cov = zeros(prod(map_size), prod(map_size));
    eng_map_cov(index_valid, index_valid) = eng_map_cov_temp;
end
clear eng_map_cov_temp;
%% Initialize map
eng_range = [min(gt_map(:)), max(gt_map(:))];

obs_map = double(gt_map >= gt_map_thres) * gt_map_thres;
obs_map_thres = gt_map_thres - 1;
eng_map_thres = gt_map_thres - 1;
weight = 1;
flag_heuristics = 0;
if flag_save_cov
    world_obj = World(obs_map, eng_map, eng_map_var, map_res, eng_map_thres, obs_map_thres, weight, flag_heuristics, eng_map_cov);
else
    world_obj = World(obs_map, eng_map, eng_map_var, map_res, eng_map_thres, obs_map_thres, weight, flag_heuristics);
end
if flag_save_map_data
    save(save_name, 'obs_map', 'eng_map', 'eng_map_var', 'map_res', 'eng_map_thres', 'obs_map_thres', 'weight', 'flag_heuristics', 'world_obj', 'gt_map', 'map_size', 'gt_map_thres', 'gpr');
end

%% Run A* agorithm
start_loc_int = [5, 43];
start_loc = start_loc_int * map_res;
goal_loc_int = [49, 11];
goal_loc = goal_loc_int * map_res;
tic;
[path_traj, path_cost, path_var1, path_var2] = world_obj.get_eng_path(start_loc, goal_loc, gpr);
toc;

fprintf('path_var1 = %f\n', path_var1);
fprintf('path_var2 = %f\n', path_var2);

%% Plots
font_size = 30;
font_name = 'Times New Roman';
line_width = 1.5;
% image_pos = [100, 100, 100, 100];

if true
    close all;
    % Ground truth energy map
    figure('units','normalized');
    imagesc(gt_map, eng_range); axis equal; colorbar; colormap jet;
    if flag_title
        title('Ground truth energy map');
    end
    map_plot_resize;

    
    % Predicted energy map
    figure('units','normalized');
    imagesc(eng_map, eng_range); axis equal; colorbar; colormap jet;
    if flag_title
        title('Predicted energy map');
    end
    map_plot_resize;
    
    % Predicted energy map uncertainty
    figure('units','normalized');
    eng_map_sigma_plot = zeros(map_size);
    eng_map_sigma_plot(index_valid) = sqrt(eng_map_var(index_valid));
    imagesc(eng_map_sigma_plot); axis equal; colorbar; colormap jet;
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    if flag_title
        title('Predicted energy map uncertainty');
    end
    map_plot_resize;

    % Error map
    figure('units','normalized');
    error_map = gt_map - eng_map;
    imagesc(abs(gt_map - eng_map)); axis equal; colorbar; colormap jet;
    if flag_title
        title('Error map');
    end
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    map_plot_resize;

    % Error %
    figure('units','normalized');
    imagesc(error_map./(eng_map_sigma_plot + 1e-6)); axis equal; colorbar; colormap jet;
    if flag_title
        title('Error (+-) %');
    end
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    map_plot_resize;

    % Error %
    error_relative = abs(error_map)./(eng_map_sigma_plot + 1e-6);
    error_99 = sort(error_relative(:));
    error_99 = error_99(ceil(0.999 * length(error_99)))

    error_relative = abs(error_map);
    error_99 = sort(error_relative(:));
    error_99 = error_99(ceil(0.999 * length(error_99)))

    
    figure('units','normalized');
    imagesc(error_relative); axis equal; colorbar; colormap jet;
    if flag_title
        title('Error %');
    end
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    map_plot_resize;
    
    % Error %
    error_map_sign = zeros(size(error_map));
    error_map_sign(error_map>0) = 1;
    error_map_sign(error_map<0) = -1;
    figure('units','normalized');
    imagesc(error_map_sign); axis equal; colorbar; colormap jet;
    if flag_title
        title('Error (+-)');
    end
    hold on;
    scatter(index_x, index_y, 'k.');
    hold off;
    map_plot_resize;
    
end

if false
    % Covariance matrix
    figure('units','normalized');
    imshow(eng_map_cov_temp, []); axis equal; colorbar; colormap jet;
    if flag_title
        title('Covariance matrix');
    end
    map_plot_resize;
    index_pixel_x_chosen_list = [30;11];
    index_pixel_y_chosen_list = [16;34];
    index_pixel_chosen_list = sub2ind(map_size, index_pixel_x_chosen_list, index_pixel_y_chosen_list);
    
    clims = [min(min(eng_map_cov_temp(:, index_pixel_chosen_list))), max(max(eng_map_cov_temp(:, index_pixel_chosen_list)))];
    % Cov matrix w.r.t pixel 1
    index_pixel_x_chosen = index_pixel_x_chosen_list(1);
    index_pixel_y_chosen = index_pixel_y_chosen_list(1);
    index_pixel_chosen = sub2ind(map_size, index_pixel_y_chosen, index_pixel_x_chosen);
    % index_pixel_chosen_in_temp = find(index_valid_ind == index_pixel_chosen)
    index_pixel_chosen_in_temp = index_valid_cumsum(index_pixel_chosen);
    img1 = zeros(map_size);
    img1(index_valid) = eng_map_cov_temp(:, index_pixel_chosen_in_temp);
    figure('units','normalized');
    imagesc(img1, clims); axis equal; colorbar; colormap jet;
    if flag_title
        title(['Covariance w.r.t. pixel (', num2str(index_pixel_x_chosen), ', ', num2str(index_pixel_y_chosen), ')']);
    end
    map_plot_resize;
    
    % Cov matrix w.r.t pixel 2
    index_pixel_x_chosen = index_pixel_x_chosen_list(2);
    index_pixel_y_chosen = index_pixel_y_chosen_list(2);
    index_pixel_chosen = sub2ind(map_size, index_pixel_y_chosen, index_pixel_x_chosen);
    % index_pixel_chosen_in_temp = find(index_valid_ind == index_pixel_chosen)
    index_pixel_chosen_in_temp = index_valid_cumsum(index_pixel_chosen);
    img2 = zeros(map_size);
    img2(index_valid) = eng_map_cov_temp(:, index_pixel_chosen_in_temp);
    figure('units','normalized');
    imagesc(img2, clims); axis equal; colorbar; colormap jet;
    if flag_title
        title(['Covariance w.r.t. pixel (', num2str(index_pixel_x_chosen), ', ', num2str(index_pixel_y_chosen), ')']);
    end
    map_plot_resize;
end

%% Save
flag_print = false;
if flag_print
    print_folder = ['./temp/'];
    for index_plot = 1:7
        thefigure = figure(index_plot);
        print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(300)]);
        % saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
    close all;
end
