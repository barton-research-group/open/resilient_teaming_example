function save_vehicle(file_name, vehicle_eng_cap, vehicle_cost, vehicle_cap)
cap_eps = 1e-6;

fileID = fopen(file_name,'w');
for index_vehicle = 1:length(vehicle_eng_cap)
    fprintf(fileID, "vehicle%d:\n", index_vehicle-1);
    fprintf(fileID, "  engCap: %f\n", vehicle_eng_cap(index_vehicle));
    fprintf(fileID, "  engCost: %f\n", vehicle_cost(index_vehicle));
    fprintf(fileID, "  capVector: [");
    for index_cap = 1:size(vehicle_cap, 2)
        if index_cap > 1
            fprintf(fileID, ", ");
        end
        fprintf(fileID, "%f", vehicle_cap(index_vehicle, index_cap));
    end
    fprintf(fileID, "]\n");
    fprintf(fileID, "  capVar: [");
    for index_cap = 1:size(vehicle_cap, 2)
        if index_cap > 1
            fprintf(fileID, ", ");
        end
        if vehicle_cap(index_vehicle, index_cap) > cap_eps
            fprintf(fileID, "%f", (vehicle_cap(index_vehicle, index_cap)*0.1)^2);
        else
            fprintf(fileID, "%f", 0);
        end
    end
    fprintf(fileID, "]\n");
end

fclose(fileID);

end