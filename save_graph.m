function save_graph(file_name, edge_v, edge_i, edge_j, edge_type, edge_cost, edge_var, edge_time_cost, n_mission)

fileID = fopen(file_name,'w');

if nargin >= 9
    n_node = max([edge_i; edge_j]);
else
    n_mission = -1;
end

n_vehicle = max(edge_v);
for index_vehicle = 1:n_vehicle
    index_this_vehicle = find(edge_v == index_vehicle);
    n_link = length(index_this_vehicle);
    fprintf(fileID, "vehicle%d:\n", index_vehicle-1);
    if n_mission > 0
        fprintf(fileID, "  nodeNum: %d\n", n_node);
        fprintf(fileID, "  startNode: %d\n", n_mission + index_vehicle - 1);
        fprintf(fileID, "  endNode: %d\n", n_mission + n_vehicle + index_vehicle - 1);
    end
    for index_link = 1:n_link
        index_row = index_this_vehicle(index_link);
        fprintf(fileID, "  edge%d: [%d, %d, %d, %f, %f, %f]\n", index_link-1, ...
            edge_i(index_row)-1, edge_j(index_row)-1, edge_type(index_row)-1, ...
            edge_cost(index_row), edge_var(index_row), edge_time_cost(index_row));
    end
end

fclose(fileID);

end