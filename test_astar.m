close all;
clear;
% clc;

%% Load data
flag_map = 4;
switch flag_map
    case 1
        map = load('data/map1.txt');
        map_thres = 100;
        start_loc = [208; 159];
        goal_loc = [341; 338];
    case 2
        map = load('data/map2.txt');
        map_thres = 6500;
        start_loc = [1369; 231];
        goal_loc = [341; 338];
    case 3
        map = load('data/map3.txt');
        map_thres = 100;
        start_loc = [45; 119];
        goal_loc = [328; 238];
    otherwise
        map = load('data/map4.txt');
        map_thres = 5000;
        start_loc = [12; 459];
        goal_loc = [247; 123];
end

flag_heuristics = 0;
weight = 1;

%% Initialize map
obs_map = map;
obs_map_thres = map_thres;
eng_map = map;
eng_map_sigma = ones(size(obs_map));
eng_map_thres = map_thres;
map_res = 10;
world_obj = World(obs_map, eng_map, eng_map_sigma, map_res, eng_map_thres, obs_map_thres, weight, flag_heuristics);

%% Run A* agorithm
tic;
% map = ones(size(map));
[path, path_cost] = mex_get_path(map, map_thres, start_loc, goal_loc, weight, flag_heuristics);
toc;
[path2, path_cost2, ~] = world_obj.get_path(start_loc * map_res, goal_loc * map_res);

figure('units','normalized','outerposition',[0 0 1 1]);
imagesc(map); axis square; colorbar; colormap jet; hold on;

scatter(start_loc(1), start_loc(2), 'g.');
scatter(goal_loc(1), goal_loc(2), 'r.');
plot(path2(:,1), path2(:,2), 'y');

hold off;
%%
[path_mu, path_cov] = world_obj.get_uncertainty_from_map(path2);


