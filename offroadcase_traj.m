close all;

vehicle_plot_list = [1,2,3]; % Plot the first three vehicle trajectories

plot_file = '../result/energy/int_v18_m14_a1/result.yaml';
mat_file = '../test/energy/int_v18_m14_a1/matlog.mat';

% plot_file = '../resilient_team_planner/build/energy_int_result.yaml';
% mat_file = '../resilient_team_planner/config/energy_int/matlog.mat';

%% Plot
flag_plot_map = true;

addpath('lib/YAMLMatlab_0.4.3/');
result = ReadYaml(plot_file);
load(mat_file);
x = cell2mat(result.xVar); x = x(:);

background_color = [0, 0, 0];
the_color = [0,0.447000000000000,0.741000000000000;0.850000000000000,0.325000000000000,0.0980000000000000;0.929000000000000,0.694000000000000,0.125000000000000;0.494000000000000,0.184000000000000,0.556000000000000;0.466000000000000,0.674000000000000,0.188000000000000;0.301000000000000,0.745000000000000,0.933000000000000;0.635000000000000,0.0780000000000000,0.184000000000000];

% eng_range = [min(gt_map(:)), max(gt_map(:))];
eng_range = [17, 43];

font_size = 30;
font_size_text = 35;
font_name = 'Times New Roman';
line_width = 1.5;
marker_size = 20;
% set(0,'defaultAxesFontSize',10)

flag_cont_plot = false;
if exist('flag_cont')
    if flag_cont
        flag_cont_plot = true;
    end
end

figure('units','normalized');
% figure(1);
ax = subplot(1,1,1);
if flag_plot_map
    if ~isempty(background_color)
        rectangle('Position',[0.5, 0.5, size(eng_map, 2), size(eng_map, 1)], 'FaceColor', background_color, 'EdgeColor','None');
        hold on;
    end
    h = imagesc(eng_map);
    colorbar; colormap summer; hold on;
    h.AlphaData = eng_map < 55;
    set(ax, 'clim', eng_range);
    set(gca, 'ydir', 'reverse');
    axis equal;
end

c_offset = 0;

vehicle_list = fieldnames(result.vehicle);

% if flag_cont_plot
type = 1;
for index_vehicle_plot = 1:length(vehicle_plot_list)
    index_vehicle = vehicle_plot_list(index_vehicle_plot);
    node_list = cell2mat(result.vehicle.(vehicle_list{index_vehicle}).node);
    for index_node = 1:length(node_list)-1
        veh_type = result.vehicle.(vehicle_list{index_vehicle}).type;
        index_x = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, veh_type, node_list(index_node), node_list(index_node+1));
        x_offset = (index_vehicle_plot - 1) * c_offset;
        y_offset = x_offset;
        plot(fpath{index_x}(:,1) + x_offset, fpath{index_x}(:,2) + y_offset, 'LineWidth', 3, 'Color', the_color(index_vehicle_plot, :));
        hold on;
    end
end
% else
%     type = 1;
%     for index_vehicle = vehicle_plot_list
%         ind_in = get_index(n_mission, n_vehicle, n_link, mission_sub2ind, type, index_vehicle, -1, -1);
%         for index_x = ind_in.'
%             if x(index_x) > 0.5
%                 plot(fpath{index_x}(:,1), fpath{index_x}(:,2), 'k', 'LineWidth', 7);
%                 hold on;
%                 plot(fpath{index_x}(:,1), fpath{index_x}(:,2), 'w--', 'LineWidth', 3);
%             end
%         end
%     end
% end

plot(start_loc_int_list(:, 1), start_loc_int_list(:, 2), 'gx', 'LineWidth', 4, 'MarkerSize', marker_size);
plot(terminal_loc_int_list(:, 1), terminal_loc_int_list(:, 2), 'gx', 'LineWidth', 4, 'MarkerSize', marker_size);
plot(mission_loc_int_list(:, 1), mission_loc_int_list(:, 2), 'rx', 'LineWidth', 4, 'MarkerSize', marker_size);

xy_offset = 0.5;

for index_mission = 1:n_mission
    t = text(mission_loc_int_list(index_mission, 1) + xy_offset, mission_loc_int_list(index_mission, 2) + xy_offset, ['${m_{', num2str(index_mission),'}}$'], 'Color', 'k', 'FontSize', font_size_text, 'FontName', font_name, 'Interpreter', 'latex');
end

hold off;
set(gca, 'ydir', 'reverse');
axis equal;

map_plot_resize;

%% Save
flag_print = true;
if flag_print
    print_folder = ['./temp/'];
    for index_plot = [1]
        thefigure = figure(index_plot);
        print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(300)]);
        % saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
end