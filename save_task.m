function save_task(file_name, mission_cap_type, mission_cap_num)

fileID = fopen(file_name,'w');

for index_mission = 1:size(mission_cap_type, 1)
    fprintf(fileID, "task%d:\n", index_mission-1);
    for index_and = 1:size(mission_cap_type, 2)
        a_and_type = mission_cap_type{index_mission, index_and};
        if isempty(a_and_type)
            break;
        end
        a_and_num = mission_cap_num{index_mission, index_and};
        fprintf(fileID, "  and%d:\n", index_and-1);
        for index_or = 1:length(a_and_type)
            fprintf(fileID, "    or%d:\n", index_or-1);
            fprintf(fileID, "      geq: true\n");
            fprintf(fileID, "      capId: %d\n", a_and_type(index_or)-1);
            fprintf(fileID, "      capReq: %f\n", a_and_num(index_or));
            fprintf(fileID, "      capVar: %f\n", (a_and_num(index_or)*0.1)^2 );
        end
    end
end

fclose(fileID);

end