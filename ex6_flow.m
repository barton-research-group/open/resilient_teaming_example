%% ARC Project Flow
close all;
% clear;
% clc;

%% Set Parametersc_dist_cost = 30;
c_dist_cost = 1;
sigma_dist_cost = 0.2;

n_mission = 35;
n_vehicle = 1;
edge_limit = 10;

%% Initialize data
mission_dist = squareform( pdist((1:n_mission).') );
mission_link_map = (mission_dist > 0);
for i = 1:n_mission
    for j = (i+1):n_mission
        if rand > 0.5
            mission_link_map(i, j) = 0;
        else
            mission_link_map(j, i) = 0;
        end
        if rand > 0.5
            mission_link_map(i, j) = 0;
            mission_link_map(j, i) = 0;
        end
    end
end

[mission_from, mission_to] = find(mission_link_map);
mission_sub2ind = sparse(mission_from, mission_to, (1:size(mission_from,1)).', n_mission, n_mission);
mission_ind2sub = [mission_from, mission_to];
n_link = size(mission_ind2sub, 1);

edge_time_cost_max = ones((n_link + 2*n_mission) * n_vehicle, 1) * edge_limit * n_link;
edge_time_cost = rand((n_link + 2*n_mission) * n_vehicle, 1) * edge_limit;
mission_time_cost = ones(n_vehicle, n_mission);
start_time = zeros(n_vehicle, 1);

n_xvar = (n_link + 2*n_mission) * n_vehicle;

%% Gnerate Map
edge_cost = zeros(n_xvar, 1);
edge_var = zeros(n_xvar, 1);
fpath = cell(n_xvar, 1);
edge_v = zeros(n_xvar, 1);
edge_i = zeros(n_xvar, 1);
edge_j = zeros(n_xvar, 1);
edge_type = ones(n_xvar, 1);
ex6_map

%% Generate a flow graph
node_names = (1:(n_vehicle*2+n_mission)).';
node_code = cell(length(node_names),1);
for i = 1:length(node_names)
    node_code{i} = num2str(node_names(i));
end
start_node = n_mission+1;
end_node = n_mission+n_vehicle+1;
node_code{start_node} = 'S';
node_code{end_node} = 'U';

% node_table = table(node_code, node_code,'VariableNames',{'Name', 'Code'});

% edge_code = num2cell(edge_time_cost);
% edge_table = table([edge_i, edge_j], edge_time_cost, 'VariableNames',{'EndNodes', 'Weight'});

% G = digraph(edge_table, node_table);
G = digraph(edge_i, edge_j, edge_time_cost);

% H = plot(G,'EdgeLabel',G.Edges.Weight,'Layout','layered');
[mf,GF] = maxflow(G, start_node, end_node,'augmentpath');

% H.EdgeLabel = {};
% highlight(H,GF,'EdgeColor','r','LineWidth',2);
% st = GF.Edges.EndNodes;
% labeledge(H,st(:,1),st(:,2),GF.Edges.Weight);

% figure(1);
% H1 = plot(GF,'EdgeLabel',GF.Edges.Weight, 'NodeLabel', node_code,'Layout','layered');


edge_i2 = GF.Edges.EndNodes(:,1);
edge_j2 = GF.Edges.EndNodes(:,2);

[index_ba, n_iter] = find_b_in_a([edge_i, edge_j], GF.Edges.EndNodes);

edge_v2 = edge_v(index_ba);

edge_type2 = edge_type(index_ba);
edge_cost2 = edge_cost(index_ba);
edge_var2 = edge_var(index_ba);
edge_time_cost2 = edge_time_cost(index_ba);
edge_flow_max2 = edge_time_cost_max(index_ba);
edge_flow = GF.Edges.Weight;

n_ori_edge = length(edge_i2);
test_case_row = [n_mission, edge_limit, n_ori_edge, mf];

%% Save graph
mat_test_folder = '../test/';
cpp_test_folder = '../../test/';
if exist('i_loop','var') == 1
    sub_folder = ['flow_rounder2/set1/', 'm', num2str(n_mission), '_elim', num2str(edge_limit), '/repeat', num2str(i_loop), '/'];
else
    sub_folder = 'flow_rounder2/2/';
end
mkdir([mat_test_folder, sub_folder]);

graph_file = [mat_test_folder, sub_folder, 'graph.yaml'];
save_graph(graph_file, edge_v2, edge_i2, edge_j2, edge_type2, edge_cost2, edge_flow_max2, edge_flow, n_mission);


%% Show directory
mat_file = [mat_test_folder, sub_folder, 'matlog.mat'];
save(mat_file);
mat_result_folder = '../result/';
cpp_result_folder = '../../result/';
mkdir([mat_result_folder, sub_folder]);

ccp_graph_file = [cpp_test_folder, sub_folder, 'graph.yaml'];
round_result_file = [cpp_result_folder, sub_folder, 'round.yaml'];
cover_result_file = [cpp_result_folder, sub_folder, 'cover.yaml'];
fprintf("./flow_main %s %s %s\n", ccp_graph_file, round_result_file, cover_result_file);
