function save_plannerparam(file_name, folder, n_vehicle, n_mission, n_cap, n_av, n_vehicle_rep, cap_cumulative, flag_cont)

fileID = fopen(file_name,'w');

if nargin < 7
    n_vehicle_rep = 1;
end
if nargin < 8
    cap_cumulative = zeros(1, n_cap);
end
if nargin < 9
    flag_cont = false;
end

fprintf(fileID, "flagOptimizeCost: true\n");
fprintf(fileID, "flagTaskComplete: true\n");
fprintf(fileID, "flagSprAddCutToSameType: true\n");
fprintf(fileID, "taskCompleteReward: 100\n");
fprintf(fileID, "timePenalty: 1.0\n");
fprintf(fileID, "recoursePenalty: 1.0\n");
fprintf(fileID, "taskRiskPenalty: 800.0\n");
fprintf(fileID, "LARGETIME: 10000.0\n");
fprintf(fileID, "MAXTIME: 1000.0\n");
fprintf(fileID, "MAXENG: 1E8\n");
if flag_cont
    fprintf(fileID, "flagSolver: TEAMPLANNER_CONDET\n");
else
    fprintf(fileID, "flagSolver: TEAMPLANNER_DET\n");
end
fprintf(fileID, "CcpBeta: 0.95\n");
fprintf(fileID, "taskBeta: 0.95\n");
fprintf(fileID, "solverMaxTime: 120.0\n");
fprintf(fileID, "solverIterMaxTime: 50.0\n");
fprintf(fileID, "flagNotUseUnralavant: true\n");
fprintf(fileID, "MAXALPHA: 20.0\n");
fprintf(fileID, "taskNum: %d\n", n_mission);
fprintf(fileID, "vehNum: %d\n", n_vehicle);
fprintf(fileID, "capNum: %d\n", n_cap);
fprintf(fileID, "vehTypeNum: %d\n", n_av);

fprintf(fileID, "vehNumPerType: [");
for index_av = 1:n_av
    if (index_av > 1)
        fprintf(fileID, ", ");
    end
    fprintf(fileID, "%d", n_vehicle_rep);
end
fprintf(fileID, "]\n");

fprintf(fileID, "sampleNum: %d\n", 500);
fprintf(fileID, "randomType: %d\n", 0);

fprintf(fileID, "capType: [");
for index_cap = 1:n_cap
    if (index_cap > 1)
        fprintf(fileID, ", ");
    end
    fprintf(fileID, "%d", cap_cumulative(index_cap));
end
fprintf(fileID, "]\n");

fprintf(fileID, "vehicleParamFile: %s\n", [folder, 'vehicle_param.yaml']);
fprintf(fileID, "taskParamFile: %s\n", [folder, 'task_param.yaml']);
fprintf(fileID, "graphFile: %s\n", [folder, 'graph.yaml']);

fclose(fileID);

end