

n_pose = size(pose_data, 1);
n_edge = size(edge_data, 1);
edge_cost = edge_dist .* edge_unit_eng;
G = digraph(edge_data(:,1), edge_data(:,2), edge_cost);
edge_id = sparse(edge_data(:,1), edge_data(:,2), 1:n_edge, n_pose, n_pose);

%% Test connectivity
bins = conncomp(G);
flag_connect = sum(bins ~= 1) == 0;

%% Run the shortest path algorithm
start = 228;
% start = 1;
% goal = 433; % 1122; 1061;
goal = randi(n_pose);
[path_node, path_edge, path_dist, path_cost, path_cost_var] = my_getpath(G, start, goal, edge_id, edge_dist, edge_unit_eng_cov);

path_pose = pose_data(path_node, :);

%% Show the path
figure(1);
eng_range = [10, 100];
imagesc(eng_map, eng_range); axis equal; colorbar;
median_range = [min(eng_map(:)), eng_map_prior_mean-eng_map_margin, eng_map_prior_mean+eng_map_margin, max(eng_map(:))];
color_map = my_colormap(1, median_range);
colormap(color_map);
hold on;
plot(pose_data(:,1), pose_data(:,2), 'k.');
plot(path_pose(:,1), path_pose(:,2), 'LineWidth', line_width);
plot(path_pose(1,1), path_pose(1,2), 'gx', 'LineWidth', line_width);
plot(path_pose(end,1), path_pose(end,2), 'rx', 'LineWidth', line_width);
hold off;
%     colormap jet;
set(gca, 'ydir', 'normal'); % reverse, normal
map_plot_resize;

%% Save
flag_print = true;
if flag_print
    %pause;
    print_folder = ['./temp/'];
    for index_plot = 1
        thefigure = figure(index_plot);
                print(thefigure, [print_folder,'figure',num2str(index_plot),'.png'],'-dpng',['-r' num2str(300)]);
%         saveas(thefigure, [print_folder,'figure',num2str(index_plot),'.pdf']);
    end
%     close all;
end
